import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { ScheduleClients, SchedulerJobExecuteLogModel } from 'app/shared/api-clients/schedule.client';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NotificationService } from 'app/shared/services/notification.service';
import { WidthColumn } from 'app/shared/configs/width-column';
import { TypeColumn } from 'app/shared/configs/type-column';
import { IndexColumn } from 'app/shared/configs/index-column';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FilesClient, TemplateType } from 'app/shared/api-clients/files.client';
import { ImportService } from 'app/shared/services/import.service';
import { EventType } from 'app/shared/enumerations/import-event-type.enum';

@Component({
  templateUrl: './scheduler-monitoring.component.html',
  styleUrls: ['./scheduler-monitoring.component.scss'],
})
export class SchedulesLogComponent implements OnInit, OnDestroy {
  title = 'Scheduler Monitoring';

  expandedItems: any[] = [];

  selectedItem: SchedulerJobExecuteLogModel;
  schedules: SchedulerJobExecuteLogModel[] = [];

  scheduleAffterMap = {
    id: '',
    jobKey: '',
    jobName: '',
    jobFullName: '',
    startTime: Date,
    endTime: Date,
    totalCovarageQty: 0,
    totalErrorQty: 0,
    totalSuccessQty: 0,
    index: 0,
  };

  scheduleAffterMapIndex: any[] = [];

  editMode = false;
  deviceMachineForm: FormGroup;

  titleDialog: string;
  dialog = false;

  WidthColumn = WidthColumn;
  TypeColumn = TypeColumn;
  IndexColumn = IndexColumn;

  cols: any[] = [];
  fields: any[] = [];

  ref: DynamicDialogRef;
  destroyed$ = new Subject<void>();

  get factoryNameControl() {
    return this.deviceMachineForm.get('factoryName');
  }

  get deviceCodeControl() {
    return this.deviceMachineForm.get('deviceCode');
  }

  get ipAddressControl() {
    return this.deviceMachineForm.get('ipAddress');
  }

  get portNumberControl() {
    return this.deviceMachineForm.get('portNumber');
  }

  get notesControl() {
    return this.deviceMachineForm.get('notes');
  }

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private confirmationService: ConfirmationService,
    private dialogService: DialogService,
    private filesClient: FilesClient,
    private scheduleClient: ScheduleClients,
    private importService: ImportService
  ) {}

  ngOnInit() {
    this.cols = [
      //{ header: '', field: 'checkBox', width: WidthColumn.CheckBoxColumn, type: TypeColumn.CheckBoxColumn },
      { header: 'Index', field: '', width: WidthColumn.IdentityColumn, type: TypeColumn.IdentityColumn },
      { header: 'ID', field: 'id', width: WidthColumn.GuidIdColumn, type: TypeColumn.NormalColumn },
      // { header: 'Job Full Name', field: 'jobFullName', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      // { header: 'Job Key', field: 'jobKey', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Job Name', field: 'jobName', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Start Time', field: 'startTime', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
      { header: 'End Time', field: 'endTime', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
      { header: 'Total Covarage Qty', field: 'totalCovarageQty', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Total Error Qty', field: 'totalErrorQty', width: WidthColumn.QuantityColumn, type: TypeColumn.NormalColumn },
      { header: 'Total Success Qty', field: 'totalSuccessQty', width: WidthColumn.QuantityColumn, type: TypeColumn.NormalColumn },
      { header: 'Last Modified', field: 'lastModified', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
      { header: 'Last Modified By', field: 'lastModifiedBy', width: WidthColumn.QuantityColumn, type: TypeColumn.NormalColumn },
    ];

    this.fields = this.cols.map((i) => i.field);
    this.initForm();
    // this.initProducts();
    this.initDeviceMachines();
    this.initEventBroadCast();
  }

  initEventBroadCast() {
    this.importService.getEvent().subscribe((event) => {
      switch (event) {
        case EventType.HideDialog:
          if (this.ref) {
            this.ref.close();
          }
          this.initDeviceMachines();
          break;
      }
    });
  }

  initForm() {
    this.deviceMachineForm = this.fb.group({
      id: [0],
      factoryName: ['', [Validators.required]],
      deviceCode: ['', [Validators.required]],
      ipAddress: ['', [Validators.required]],
      portNumber: ['', [Validators.required, Validators.min(0)]],
      notes: [''],
      lastModifiedBy: [''],
      lastModified: [null],
    });
  }

  initDeviceMachines() {
    // this.scheduleClient
    //   .getScheduleJobExecuteLog()
    //   .pipe(takeUntil(this.destroyed$))
    //   .subscribe(
    //     (data) => ((this.schedules = data), this.mapIndex(data)),
    //     (_) => (this.schedules = [])
    //   );
    this.scheduleClient
      .getScheduleJobExecuteLog()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (i) => {
          this.scheduleAffterMapIndex = i;

          // if (this.selectedManualControl) {
          //   this.selectedManualControl = this.manualControl.find((s) => s.id === this.selectedManualControl.id);
          // }
        },
        (_) => (this.scheduleAffterMapIndex = [])
      );
  }

  openCreateDialog() {
    this.deviceMachineForm.reset();
    this.titleDialog = 'Add New Device';
    this.editMode = false;
    this.dialog = true;
  }

  hideDialog() {
    this.dialog = false;
    this.deviceMachineForm.reset();
  }

  mapIndex(data: any) {
    if (data.length > 0) {
      data.forEach((element, index) => {
        this.scheduleAffterMap = element;
        this.scheduleAffterMap.index = index + 1;
        this.scheduleAffterMapIndex.push(this.scheduleAffterMap);
      });
    }
  }

  // editProduct(deviceMachine: DeviceMachineModel) {
  //   this.titleDialog = 'Edit DeviceMachine';
  //   this.deviceMachineForm.patchValue(deviceMachine);
  //   this.editMode = true;
  //   this.dialog = true;
  // }

  // deleteProduct(deviceMachine: DeviceMachineModel) {
  //   this.confirmationService.confirm({
  //     message: 'Do you confirm to delete this item?',
  //     header: 'Confirm',
  //     icon: 'pi pi-exclamation-triangle',
  //     accept: () => {
  //       this.deviceMachineClient
  //         .deleteDeviceMachineAysnc(deviceMachine.id)
  //         .pipe(takeUntil(this.destroyed$))
  //         .subscribe(
  //           (result) => {
  //             if (result && result.succeeded) {
  //               this.notificationService.success('Delete DeviceMachine Successfully');
  //               this.selectedItem = null;
  //               this.initDeviceMachines();
  //             } else {
  //               this.notificationService.error(result?.error);
  //             }
  //           },
  //           (_) => this.notificationService.error('Delete DeviceMachine Failed. Please try again')
  //         );
  //     },
  //   });
  // }

  // onSubmit() {
  //   if (this.deviceMachineForm.invalid) {
  //     return;
  //   }

  //   this.editMode ? this.handleUpdateProduct() : this.handleAddProduct();
  // }

  // handleAddProduct() {
  //   const model = this.deviceMachineForm.value as DeviceMachineModel;
  //   model.id = 0;

  //   this.deviceMachineClient
  //     .addDeviceMachine(model)
  //     .pipe(takeUntil(this.destroyed$))
  //     .subscribe(
  //       (result) => {
  //         if (result && result.succeeded) {
  //           this.notificationService.success('Add DeviceMachine Successfully');
  //           this.initDeviceMachines();
  //           this.hideDialog();
  //         } else {
  //           this.notificationService.error(result?.error);
  //         }
  //       },
  //       (_) => this.notificationService.error('Add DeviceMachine Failed. Please try again')
  //     );
  // }

  // handleUpdateProduct() {
  //   const id = this.deviceMachineForm.value.id;
  //   this.deviceMachineClient
  //     .updateDeviceMachine(this.deviceMachineForm.value.id, this.deviceMachineForm.value)
  //     .pipe(takeUntil(this.destroyed$))
  //     .subscribe(
  //       (result) => {
  //         if (result && result.succeeded) {
  //           this.notificationService.success('Update DeviceMachine Successfully');
  //           this.initDeviceMachines();
  //           this.hideDialog();

  //           this.deviceMachineClient
  //             .getDeviceMachinesByKey(id)
  //             .pipe(takeUntil(this.destroyed$))
  //             .subscribe(
  //               (machine) => (this.selectedItem = machine),
  //               (_) => (this.selectedItem = null)
  //             );

  //         } else {
  //           this.notificationService.error(result?.error);
  //         }
  //       },
  //       (_) => this.notificationService.error('Update DeviceMachine Failed. Please try again')
  //     );
  // }

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();

    if (this.ref) {
      this.ref.close();
    }
  }
}
