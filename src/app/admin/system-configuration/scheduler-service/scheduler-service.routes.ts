import { Routes } from '@angular/router';
import { SchedulerServiceComponent } from './scheduler-service.component';

export const configurationRoutes: Routes = [{ path: '', component: SchedulerServiceComponent }];
