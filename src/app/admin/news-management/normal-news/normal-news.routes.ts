import { Routes } from '@angular/router';
import { NormalNewsComponent } from './normal-news.component';

export const normalNewsRoutes: Routes = [{ path: '', component: NormalNewsComponent }];
