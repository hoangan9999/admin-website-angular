import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  AttendanceLogClients,
  AttendanceLogModel,
  PurgingHistoryClients,
  PurgingHistoryModel
} from 'app/shared/api-clients/attendancemanagement.client';
import { AttendanceClients,AttendanceLogReportModel } from 'app/shared/api-clients/schedule.client';
import { TypeColumn } from 'app/shared/configs/type-column';
import { WidthColumn } from 'app/shared/configs/width-column';
import { HistoryDialogType } from 'app/shared/enumerations/history-dialog-type.enum';
import { EventType } from 'app/shared/enumerations/import-event-type.enum';
import { ApplicationUser } from 'app/shared/models/application-user';
import { AuthenticationService } from 'app/shared/services/authentication.service';
import { NotificationService } from 'app/shared/services/notification.service';
import { PrintService } from 'app/shared/services/print.service';
import { ConfirmationService } from 'primeng/api';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-purging-history',
  templateUrl: './purging-history.component.html',
  styleUrls: ['./purging-history.component.scss'],
})
export class PurgingHistoryComponent implements OnInit, OnDestroy {
  title = 'Purging History';
  titleDialog = '';
  titleDialogUnstufff = '';

  user: ApplicationUser;
  purgingHistory: AttendanceLogReportModel[] = [];
  selectedPurgingHistory: PurgingHistoryModel;
  attendanceLog: AttendanceLogModel[] = [];

  receivedMarkForm: FormGroup;

  isEdit = false;
  isShowDialog = false;
  isShowDialogCreate = false;
  isShowDialogHistory = false;
  isShowDialogUnstuff = false;
  isShowDialogDetail = false;
  isShowDialogEdit = false;
  isShowDialogMergeDetail = false;

  canRePrint = false;

  cols: any[] = [];
  fields: any[] = [];
  TypeColumn = TypeColumn;
  HistoryDialogType = HistoryDialogType;

  expandedItems: any[] = [];
  printData: any;

  private destroyed$ = new Subject<void>();

  constructor(
    public printService: PrintService,
    private purgingHistoryClients: PurgingHistoryClients,
    private attendanceClients: AttendanceClients,
    private notificationService: NotificationService,
    private confirmationService: ConfirmationService,
    private authenticationService: AuthenticationService,
    private fb: FormBuilder,
    private attendanceLogClients: AttendanceLogClients
  ) { }

  ngOnInit() {
    this.authenticationService.user$.pipe(takeUntil(this.destroyed$)).subscribe((user: ApplicationUser) => (this.user = user));

    this.cols = [
      //{ header: '', field: '', width: WidthColumn.IdentityColumn, type: TypeColumn.ExpandColumn },
      // { header: '', field: 'checkBox', width: WidthColumn.CheckBoxColumn, type: TypeColumn.CheckBoxColumn },
      { header: 'Job Name', field: 'schedulerJobExecuteLog',subField:"jobName", width: WidthColumn.NormalColumn, type: TypeColumn.SubFieldColumn },
      { header: 'Purge Date', field: 'startTime', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
      //{ header: 'Start Time', field: 'startTime', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
      //{ header: 'End Time', field: 'endTime', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },

      { header: 'Device ID', field: 'deviceId', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Device Code', field: 'deviceCode', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Total Record', field: 'totalLogs', width: WidthColumn.QuantityColumn, type: TypeColumn.NormalColumn }
    ];

    this.fields = this.cols.map((i) => i.field);

    this.initForm();
    this.initReceivedMarks();
    this.canRePrint = this.printService.canRePrint(this.user);
  }

  initForm() {
    this.receivedMarkForm = this.fb.group({
      id: [0],
      notes: [''],
      lastModifiedBy: [''],
      lastModified: [null],
      movementRequests: [[], [Validators.required]],
      receivedMarkMovements: this.fb.array([]),
    });
  }

  initReceivedMarks() {
    this.expandedItems = [];
    this.attendanceClients
      .getAttendanceGroupBySchedulerJobAndDate()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (i) => {
          console.log(i);
          this.purgingHistory = i;
        },
        (_) => (this.purgingHistory = [])
    );
    /*
    this.purgingHistoryClients
      .getPurgchingHistory()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (i) => {
          console.log(i);
          this.purgingHistory = i;
        },
        (_) => (this.purgingHistory = [])
    );*/
  }

  intMovementRequest() {
    this.attendanceLogClients
      .getAttendanceLog()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (i) => (this.attendanceLog = i),
        (_) => (this.attendanceLog = [])
      );
  }


  openCreateDialog() {
    this.receivedMarkForm.reset();
    this.titleDialog = 'Create Received Mark';
    this.isShowDialogCreate = true;
    this.isEdit = false;
  }



  onPrint() {
    this.printService.printDocument('received-mark');
  }

  openHistoryDialog() {
    this.isShowDialogHistory = true;
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }
}

export interface PurgingHistoryModel1 extends PurgingHistoryModel {
  isEditRow?: boolean;
}
