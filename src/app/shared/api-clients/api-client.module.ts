import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { API_BASE_URL as USER_BASE_URL, UserClient } from './user.client';
import { API_BASE_URL as FILES_BASE_URL, FilesClient } from './files.client';
import {
  API_BASE_URL as SHIPPING_BASE_URL,
  ConfigClients,
  CountryClients,
  DeviceMachineClients,
  AttendanceLogClients,
  AttendanceSummaryClients,
  PurgingHistoryClients,
  ErrorsLogClients,
  SchedulerSettingClients,
  ManualControlClients,
  IntegrationHistoryClients,
} from './attendancemanagement.client';

import {NewsClient} from './news.client';

import { ErrorLogsClients, ScheduleClients, AttendanceClients } from './schedule.client';
import { environment } from 'environments/environment';
import { API_BASE_URL as COMMUNICATIONS_BASE_URL, CommunicationClient } from './communications.client';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [
    NewsClient,
    UserClient,
    FilesClient,
    ConfigClients,
    CountryClients,
    AttendanceLogClients,
    AttendanceSummaryClients,
    IntegrationHistoryClients,
    PurgingHistoryClients,
    ErrorsLogClients,
    ManualControlClients,
    SchedulerSettingClients,
    CommunicationClient,
    DeviceMachineClients,
    ScheduleClients,
    ErrorLogsClients,
    AttendanceClients,
    { provide: USER_BASE_URL, useValue: environment.baseUrl },
    { provide: SHIPPING_BASE_URL, useValue: environment.baseUrl },
    { provide: FILES_BASE_URL, useValue: environment.baseUrl },
    { provide: COMMUNICATIONS_BASE_URL, useValue: environment.baseUrl },
  ],
})
export class ApiClientModule { }
