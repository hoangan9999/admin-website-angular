import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'app/shared/shared.module';
import { CodeGenerationComponent } from './code-generation.component';
import { RouterModule } from '@angular/router';
import { codeGenerationRoutes } from './code-generation.routes';

@NgModule({
  imports: [CommonModule, SharedModule, RouterModule.forChild(codeGenerationRoutes)],
  declarations: [CodeGenerationComponent],
  exports: [CodeGenerationComponent],
})
export class CodeGenerationModule { }
