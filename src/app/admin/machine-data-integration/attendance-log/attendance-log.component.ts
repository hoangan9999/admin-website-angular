import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConfirmationService, SelectItem } from 'primeng/api';
import { AttendanceClients, AttendanceLogModel } from 'app/shared/api-clients/schedule.client';
import { NotificationService } from 'app/shared/services/notification.service';
import { WidthColumn } from 'app/shared/configs/width-column';
import { TypeColumn } from 'app/shared/configs/type-column';
import { HistoryDialogType } from 'app/shared/enumerations/history-dialog-type.enum';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  templateUrl: './attendance-log.component.html',
  styleUrls: ['./attendance-log.component.scss'],
})
export class AttendanceLogComponent implements OnInit, OnDestroy {
  title = 'Attendance Log';

  attendanceLogModel: AttendanceLogModel[] = [];
  selectedAttendanceLog: AttendanceLogModel;
  movementRequestForm: FormGroup;

  attendanceLogModelAffterMap = {
    deviceId : 0,
    deviceTime : Date,
    employeeId : '',
    machineSerial : '',
    inoutMode : 0,
    verifyMode : 0,
    specifiedMode : 0,
    note : '',
    index : 0, 
  }

  attendanceLogModelAffterMapIndex: any[] = [];


  isEdit = false;
  isShowDialogCreate = false;
  isShowDialogEdit = false;
  isShowDialogHistory = false;
  titleDialog = '';

  cols: any[] = [];
  colFields = [];

  TypeColumn = TypeColumn;
  HistoryDialogType = HistoryDialogType;

  selectItems: SelectItem[] = [];
  expandedItems: any[] = [];

  private destroyed$ = new Subject<void>();

  constructor(
    private notificationService: NotificationService,
    private attendanceLogClients: AttendanceClients,
    private confirmationService: ConfirmationService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.cols = [
      // { header: '', field: '', width: WidthColumn.IdentityColumn, type: TypeColumn.ExpandColumn },
      { header: 'Index', field: '', width: WidthColumn.IdentityColumn, type: TypeColumn.IdentityColumn },
      { header: 'Device ID', field: 'deviceId', width: WidthColumn.QuantityColumn, type: TypeColumn.NormalColumn },
      { header: 'Machine Serial', field: 'machineSerial', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Device Time', field: 'deviceTime',  width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
      { header: 'Employee ID', field: 'employeeId', width: WidthColumn.QuantityColumn, type: TypeColumn.NormalColumn },
      { header: 'InoutMode', field: 'inoutMode', width: WidthColumn.QuantityColumn, type: TypeColumn.NormalColumn },
      
      { header: 'Specified Mode', field: 'specifiedMode', width: WidthColumn.QuantityColumn, type: TypeColumn.NormalColumn },
      { header: 'Verify Mode', field: 'verifyMode', width: WidthColumn.QuantityColumn, type: TypeColumn.NormalColumn },
      { header: 'Note', field: 'note', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Updated Time', field: 'lastModified', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
    ];

    this.colFields = this.cols.map((i) => i.field);

    this.initAttendanceLogs();
    this.initForm();
  }


  initAttendanceLogs() {
    this.expandedItems = [];

    this.attendanceLogClients
      .getAttendanceLog()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (data) => (
          this.attendanceLogModel = data
          //this.mapIndex(data)
          ),
        // (_) => (this.attendanceLogModelAffterMapIndex = [])
        (_) => (this.attendanceLogModel = [])
      );
  }

  mapIndex(data: any){
    if(data.length > 0){
      data.forEach((element,index) => {
        this.attendanceLogModelAffterMap = element;
        this.attendanceLogModelAffterMap.index = index + 1;
        this.attendanceLogModelAffterMapIndex.push(this.attendanceLogModelAffterMap);
      });
    }
  }

  initForm() {
    this.movementRequestForm = this.fb.group({
      id: [0],
      notes: [''],
      lastModifiedBy: [''],
      lastModified: [null],
      movementRequestDetails: this.fb.array([]),
      workOrders: [[], [Validators.required]],
    });
  }

  

  openCreateDialog() {
    this.titleDialog = 'Create Movement Request';
    this.isShowDialogCreate = true;
    this.isEdit = false;
  }

  hideDialog() {
    this.isShowDialogCreate = false;
    this.isShowDialogEdit = false;
    this.isShowDialogHistory = false;
    this.selectItems = null;
    this.movementRequestForm.reset();
  }

  openHistoryDialog() {
    this.isShowDialogHistory = true;
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }
}
