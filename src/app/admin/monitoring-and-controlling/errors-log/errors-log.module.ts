import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ErrorsLogComponent } from './errors-log.component';
import { errorsLogRoutes } from './errors-log.routes';

@NgModule({
  declarations: [ErrorsLogComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(errorsLogRoutes)],
  exports: [ErrorsLogComponent],
})
export class ErrorsLogModule {}
