import { Routes } from '@angular/router';
import { ErrorsLogComponent } from './errors-log.component';

export const errorsLogRoutes: Routes = [{ path: '', component: ErrorsLogComponent }];
