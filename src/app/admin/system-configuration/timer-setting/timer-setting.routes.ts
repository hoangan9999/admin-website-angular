import { Routes } from '@angular/router';
import { TimerSettingComponent } from './timer-setting.component';

export const configurationRoutes: Routes = [{ path: '', component: TimerSettingComponent }];
