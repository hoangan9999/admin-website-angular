import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { AttendanceLogComponent } from './attendance-log.component';
import { attendanceLogRoutes } from './attendance-log.routes';

@NgModule({
  declarations: [AttendanceLogComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(attendanceLogRoutes)],
  exports: [AttendanceLogComponent],
})
export class MovementRequestModule {}
