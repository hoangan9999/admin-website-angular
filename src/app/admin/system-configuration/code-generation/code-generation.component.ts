import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { DeviceMachineClients, DeviceMachineModel, CodeGenerationModel } from 'app/shared/api-clients/attendancemanagement.client';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NotificationService } from 'app/shared/services/notification.service';
import { WidthColumn } from 'app/shared/configs/width-column';
import { TypeColumn } from 'app/shared/configs/type-column';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FilesClient, TemplateType } from 'app/shared/api-clients/files.client';
import { ImportService } from 'app/shared/services/import.service';
import { EventType } from 'app/shared/enumerations/import-event-type.enum';

@Component({
  templateUrl: './code-generation.component.html',
})
export class CodeGenerationComponent implements OnInit, OnDestroy {
  title = 'Code Generation';

  selectedItem: DeviceMachineModel;
  products: DeviceMachineModel[] = [];

  editMode = false;
  deviceMachineForm: FormGroup;

  titleDialog: string;
  dialog = false;

  WidthColumn = WidthColumn;
  TypeColumn = TypeColumn;

  cols: any[] = [];
  fields: any[] = [];

  ref: DynamicDialogRef;
  destroyed$ = new Subject<void>();

  get ipAddressControl() {
    return this.deviceMachineForm.get('ipAddress');
  }

  get privateCodeControl() {
    return this.deviceMachineForm.get('privateCode');
  }

  get activeCodeControl() {
    return this.deviceMachineForm.get('activeCode');
  }

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private confirmationService: ConfirmationService,
    private dialogService: DialogService,
    private filesClient: FilesClient,
    private deviceMachineClient: DeviceMachineClients,
    private importService: ImportService
  ) {}

  ngOnInit() {
    this.cols = [
      { header: '', field: 'checkBox', width: WidthColumn.CheckBoxColumn, type: TypeColumn.CheckBoxColumn },
      { header: 'IP', field: 'ipAddress', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Active Code', field: 'activeCode', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Updated By', field: 'lastModifiedBy', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Updated Time', field: 'lastModified', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
    ];

    this.fields = this.cols.map((i) => i.field);
    this.initForm();
    this.initDeviceMachines();
    this.initEventBroadCast();
  }

  initEventBroadCast() {
    this.importService.getEvent().subscribe((event) => {
      switch (event) {
        case EventType.HideDialog:
          if (this.ref) {
            this.ref.close();
          }
          this.initDeviceMachines();
          break;
      }
    });
  }

  initForm() {
    this.deviceMachineForm = this.fb.group({
      id: [0],
      ipAddress: ['', [Validators.required]],
      privateCode: ['', [Validators.required]],
      activeCode: [''],
      lastModifiedBy: [''],
      lastModified: [null],
    });
  }

  initDeviceMachines() {
    this.deviceMachineClient
      .getDeviceMachines()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (data) => (this.products = data),
        (_) => (this.products = [])
      );
  }

  openCreateDialog() {
    this.deviceMachineForm.reset();
    this.titleDialog = 'Generate Active Code';
    this.editMode = false;
    this.dialog = true;
  }

  hideDialog() {
    this.dialog = false;
    this.deviceMachineForm.reset();
  }

  generateActiveCode(deviceMachine: DeviceMachineModel) {
    this.titleDialog = 'Update Active Code';
    this.deviceMachineForm.patchValue(deviceMachine);
    this.editMode = true;
    this.dialog = true;
  }

  onSubmit() {
    if (this.deviceMachineForm.invalid) {
      return;
    }

    this.handleAddProduct();
  }

  handleAddProduct() {
    const model = this.deviceMachineForm.value as CodeGenerationModel;
    model.id = 0;
    this.deviceMachineClient
      .generateCode(model)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (result) => {
          if (result && result.succeeded) {
            this.notificationService.success('Generate the active code successfully');
            this.initDeviceMachines();
            this.hideDialog();
          } else {
            this.notificationService.error(result?.error);
          }
        },
        (_) => this.notificationService.error('Failed to generate the active code. Please try again')
      );
  }

  focusOutFunction() {}

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();

    if (this.ref) {
      this.ref.close();
    }
  }
}
