import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'app/shared/shared.module';
import { SchedulesLogComponent } from './scheduler-monitoring.component';
import { RouterModule } from '@angular/router';
import { SchedulesLogRoutes } from './scheduler-monitoring.routes';

@NgModule({
  imports: [CommonModule, SharedModule, RouterModule.forChild(SchedulesLogRoutes)],
  declarations: [SchedulesLogComponent],
  exports: [SchedulesLogComponent],
})
export class SchedulesLogModule { }
