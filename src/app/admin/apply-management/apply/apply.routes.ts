import { Routes } from '@angular/router';
import { ApplyComponent } from './apply.component';

export const applyRoutes: Routes = [{ path: '', component: ApplyComponent }];
