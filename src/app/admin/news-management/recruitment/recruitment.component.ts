import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NotificationService } from 'app/shared/services/notification.service';
import { WidthColumn } from 'app/shared/configs/width-column';
import { TypeColumn } from 'app/shared/configs/type-column';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { EventType } from 'app/shared/enumerations/import-event-type.enum';
import { NewsClient, NewsCategoryDto, NewsDto, RecruitmentDto } from 'app/shared/api-clients/news.client';
@Component({
  selector: 'app-recruitment',
  templateUrl: './recruitment.component.html',
})
export class RecruitmentComponent implements OnInit {

  title = 'Tin Tuyển Dụng';

  selectedItem: RecruitmentDto;
  recruitments: RecruitmentDto[] = [];

  


  editMode = false;
  form: FormGroup;

  titleDialog: string;
  dialog = false;

  WidthColumn = WidthColumn;
  TypeColumn = TypeColumn;

  cols: any[] = [];
  fields: any[] = [];

  ref: DynamicDialogRef;
  destroyed$ = new Subject<void>();
  variableFlie: any;


  get jobTitleControl() {
    return this.form.get('jobTitle');
  }

  get recruitQuantityControl() {
    return this.form.get('recruitQuantity');
  }

  get expirationDateControl() {
    return this.form.get('expirationDate');
  }

  get responsibilitiesControl() {
    return this.form.get('responsibilities');
  }

  get requirementsControl() {
    return this.form.get('requirements');
  }

  get salaryAndBenefitsControl() {
    return this.form.get('salaryAndBenefits');
  }


  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private confirmationService: ConfirmationService,
    private newsClient: NewsClient,
  ) { }

  ngOnInit() {
    this.cols = [
      { header: '', field: 'checkBox', width: WidthColumn.CheckBoxColumn, type: TypeColumn.CheckBoxColumn },
      { header: 'Index', field: '', width: WidthColumn.IdentityColumn, type: TypeColumn.IdentityColumn },
      { header: 'Tiêu Đề', field: 'jobTitle', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Số Lượng', field: 'recruitQuantity', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Ngày Hết Hạn', field: 'expirationDate', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
      { header: 'Create Time', field: 'created', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
      { header: 'Updated Time', field: 'lastModified', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
    ];

    this.fields = this.cols.map((i) => {
      i.field
    });
    this.initForm();
    this.initRecruitments();
    
  }



  initForm() {
    this.form = this.fb.group({
      id: ['00000000-0000-0000-0000-000000000000'],
      jobTitle: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(450)]],
      recruitQuantity: ['', [Validators.required, Validators.min(1), Validators.max(1000)]],
      expirationDate: ['', [Validators.required]],
      responsibilities: [''],
      requirements: [''],
      salaryAndBenefits: [''],
    });
  }


  initRecruitments() {
    this.newsClient
      .apiNewsRecruitmentGetallrecruitment()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (data) => (this.recruitments = data),
        (_) => (this.recruitments = [])
      );
  }

  openCreateDialog() {
    this.form.reset();
    this.titleDialog = 'Thêm Tin Tuyển Dụng';
    this.editMode = false;
    this.dialog = true;
  }

  hideDialog() {
    this.dialog = false;
    this.form.reset();
  }

  editItem(recruitments: RecruitmentDto) {
    this.titleDialog = 'Chỉnh Sửa Tin Tuyển Dụng';
    this.form.patchValue(recruitments);
    this.editMode = true;
    this.dialog = true;
  }

  deleteItem(recruitments: RecruitmentDto) {
    this.confirmationService.confirm({
      message: 'Bạn có chắc xóa ' + recruitments.jobTitle,
      header: 'Xác Nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.newsClient
          .apiNewsRecruitmentDelete(recruitments.id)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(
            (result) => {
              if (result && result.succeeded === true) {
                this.notificationService.success('Xóa Thành Công!');
                this.selectedItem = null;
                this.initRecruitments();
              } else {
                this.notificationService.error(result.errors[0]);
              }
            },
            (_) => this.notificationService.error('Có lỗi trong quá trình xóa, Vui lòng thử lại sau!')
          );
      },
    });
  }

  handleValueCkChange(value, title) {
    if (title === 'responsibilities') {
      this.form.patchValue({
        responsibilities: value,
      });
    }
    if (title === 'requirements') {
      this.form.patchValue({
        requirements: value,
      });
    }
    if (title === 'salaryAndBenefits') {
      this.form.patchValue({
        salaryAndBenefits: value,
      });
    }
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }

    this.editMode ? this.handleUpdate() : this.handleAdd();
  }

  handleAdd() {
      const model = this.form.value as RecruitmentDto;
      model.id = '00000000-0000-0000-0000-000000000000';
      this.newsClient
        .apiNewsRecruitmentPost(model)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          (result) => {
            if (result && result.succeeded == true) {
              this.notificationService.success('Thêm Thành Công!');
              this.initRecruitments();
              this.hideDialog();
            } else if (result && result.succeeded == false) {
              this.notificationService.error(result.errors[0]);
            }
            else {
              this.notificationService.error('Có lỗi trong quá trình thêm mới! Vui lòng thử lại sau!');
            }
          },
          (_) => this.notificationService.error('Có lỗi trong quá trình thêm mới! Vui lòng thử lại sau!')
        );
  }

  handleUpdate() {
    const id = this.form.value.id;
      this.newsClient
        .apiNewsRecruitmentPut(this.form.value)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          (result) => {
            if (result && result.succeeded === true) {
              this.notificationService.success('Chỉnh sửa thành công!');
              this.initRecruitments();
              this.hideDialog();
              this.newsClient
                .apiNewsRecruitmentGetrecruitmentbyid(id)
                .pipe(takeUntil(this.destroyed$))
                .subscribe(
                  (data) => (this.selectedItem = data),
                  (_) => (this.selectedItem = null)
                );
            }
            else if (result && result.succeeded === false) {
              this.notificationService.error(result.errors[0]);
            }
          },
          (_) => this.notificationService.error('Có lỗi trong quá trình chỉnh sửa, Vui lòng thử lại sau!')
        );
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();

    if (this.ref) {
      this.ref.close();
    }
  }
}
