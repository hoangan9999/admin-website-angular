import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { SchedulerServiceComponent } from './scheduler-service.component';
import { configurationRoutes } from './scheduler-service.routes';

@NgModule({
  declarations: [SchedulerServiceComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(configurationRoutes)],
  exports: [SchedulerServiceComponent],
})
export class SchedulerServiceModule { }
