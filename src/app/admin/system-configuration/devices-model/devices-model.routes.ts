import { Routes } from '@angular/router';
import { DeviceModelComponent } from './devices-model.component';

export const deviceModelRoutes: Routes = [{ path: '', component: DeviceModelComponent }];
