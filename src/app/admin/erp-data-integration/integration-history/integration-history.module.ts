import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { IntegrationHistoryComponent } from './integration-history.component';
import { integrationHistoryRoutes } from './integration-history.routes';

@NgModule({
  declarations: [IntegrationHistoryComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(integrationHistoryRoutes)],
  exports: [IntegrationHistoryComponent],
})
export class IntegrationHistoryModule {}
