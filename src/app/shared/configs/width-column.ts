export const WidthColumn = {
  CheckBoxColumn: 60,
  QuantityColumn: 150,
  IdentityColumn: 100,
  DateColumn: 250,
  DateTimeColumn: 250,
  NormalColumn: 250,
  GuidIdColumn: 300,
  DescriptionColumn: 500,
  ImagesColumn: 170
};
