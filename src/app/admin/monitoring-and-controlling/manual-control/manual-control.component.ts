import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import {CalendarModule} from 'primeng/calendar';
import { DeviceMachineClients, DeviceMachineModel } from 'app/shared/api-clients/attendancemanagement.client';
import { AttendanceClients, DownloadLogModel, Result } from 'app/shared/api-clients/schedule.client';
import { ConfirmationService } from 'primeng/api';
import { NotificationService } from 'app/shared/services/notification.service';
import { WidthColumn } from 'app/shared/configs/width-column';
import { TypeColumn } from 'app/shared/configs/type-column';
import { HistoryDialogType } from 'app/shared/enumerations/history-dialog-type.enum';
import { FilesClient, TemplateType } from 'app/shared/api-clients/files.client';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { EventType } from 'app/shared/enumerations/import-event-type.enum';
import { ImportService } from 'app/shared/services/import.service';
import Utilities from 'app/shared/helpers/utilities';

@Component({
  templateUrl: './manual-control.component.html',
  styleUrls: ['./manual-control.component.scss'],
})
export class ManualControlComponent implements OnInit, OnDestroy {
  title = 'Manual Control';
  titleDialog = '';

  manualControls: DeviceMachineModel[] = [];
  selectedManualControl: DeviceMachineModel;

  shippingPlanForm: FormGroup;

  isEdit = false;
  isShowDialog = false;
  isShowDialogHistory = false;

  cols: any[] = [];
  fields: any[] = [];

  TypeColumn = TypeColumn;
  HistoryDialogType = HistoryDialogType;

  ref: DynamicDialogRef;

  deviceMachineForPingStatus: DeviceMachineModel;
  showDialogDownload = false;
  downloadLogForm: FormGroup;
  fromTimeValue: any;
  toTimeValue: any;

  showDialogDelete = false;
  deleteLogForm: FormGroup;

  expandedItems: any[] = [];
  private destroyed$ = new Subject<void>();

  get fromTimeControl() {
    return this.downloadLogForm.get('fromTime');
  }
  get toTimeControl() {
    return this.downloadLogForm.get('toTime');
  }

  get deleteFromTimeControl(){
    return this.deleteLogForm.get('toTime');
  }

  toTime = '';
  validateSelectedDate = '*';

  constructor(
    private manualControlClients: DeviceMachineClients,
    private confirmationService: ConfirmationService,
    private notificationService: NotificationService,
    private dialogService: DialogService,
    private filesClient: FilesClient,
    private fb: FormBuilder,
    private importService: ImportService,
    private attendanceClient: AttendanceClients
  ) {}

  ngOnInit() {
    this.cols = [
      { header: '', field: 'checkBox', width: WidthColumn.CheckBoxColumn, type: TypeColumn.CheckBoxColumn },
      { header: 'Index', field: '', width: WidthColumn.IdentityColumn, type: TypeColumn.IdentityColumn },
      { header: 'IP Address', field: 'ipAddress', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Port Number', field: 'portNumber', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Last connected', field: 'lastConnect', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
      { header: 'Last Modified', field: 'lastModified', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
    ];

    this.fields = this.cols.map((i) => i.field);

    this.initForm();
    this.initShippingPlans();
    this.initEventBroadCast();
  }

  initEventBroadCast() {
    this.importService.getEvent().subscribe((event) => {
      switch (event) {
        case EventType.HideDialog:
          if (this.ref) {
            this.ref.close();
          }
          this.initShippingPlans();
          break;
      }
    });
  }

  initForm() {
    
    this.downloadLogForm = this.fb.group({
      fromTime: ['', [Validators.required]],
      toTime: ['', [Validators.required]],
    });

    this.deleteLogForm = this.fb.group({
      toTime: ['', [Validators.required]],
    });

  }

  initShippingPlans() {
    this.expandedItems = [];
    this.manualControlClients
      .getDeviceMachines()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (data) => {
          this.manualControls = data.filter((x) => x.active == true);
          // this.mapIndex(data.filter(x => x.active == true))
        }
        // (_) => (this.manualControls = [])
      );
  }

  onSelected(data) {
    this.deviceMachineForPingStatus = data;
  }

  ping(){
    this.attendanceClient.pingStatus(this.deviceMachineForPingStatus).subscribe(data =>{
      if(data){
        if(data.succeeded){
          this.notificationService.success("Ping Success !");
        }
        else{
          this.notificationService.error("Ping Fail !");
        }
      }
    });
  }

  reconnect(){
    this.attendanceClient.reconnectDevice(this.deviceMachineForPingStatus).subscribe(data =>{
      if(data){
        if(data.succeeded){
          this.notificationService.success("Reconnect Success !");
        }
        else{
          this.notificationService.error("Reconnect Fail !");
        }
      }
    });
  }
  restartDevice(){
    const listDevice: Array<DeviceMachineModel> = [];
    listDevice.push(this.deviceMachineForPingStatus);
    this.attendanceClient.restartDeviceManual(listDevice).subscribe(data =>{
      if(data){
        const listError: Array<string> = data.data;
        
        if(data.succeeded && listError.length == 0){
          this.notificationService.success("Restart device Success !");
        }
        else{
          this.notificationService.error("Restart device failed."+listError);
        }
      }
    });
  }
  downloadLog(){
    this.titleDialog = "Download log";
    this.showDialogDownload = true;
    this.downloadLogForm.reset();
  }
  hideDialog() {
    this.showDialogDownload = false;
    this.downloadLogForm.reset();
  }

  hideDialogDelete() {
    this.showDialogDelete = false;
    this.deleteLogForm.reset();
    this.toTime = ''
    this.validateSelectedDate = '*';
  }
  onDownload(){
    if(this.downloadLogForm.invalid){
      this.notificationService.error("Please complete all information !");
    }
    else{
      const model: DownloadLogModel = {
        listDevice: [this.deviceMachineForPingStatus],
        fromTime: this.convertDateBeforeSendToServer(this.downloadLogForm.value.fromTime),
        toTime: this.convertDateBeforeSendToServer(this.downloadLogForm.value.toTime)
      };
      
      this.attendanceClient.downloadLogManual(model).subscribe(data =>{
        if(data){
          const listError: Array<string> = data.data;
          
          if(data.succeeded && listError.length == 0){
            this.notificationService.success("Download log success !");
            this.showDialogDownload = false;
          }
          else{
            this.notificationService.error("Download log failed.<br/>"+listError);
          }
        }
      });
    }
  }

  deleteLog(){
    this.titleDialog = "Delete log";
    this.showDialogDelete = true;
    
    //this.downloadLogForm.reset();
  }

  ondelete(){
    let currentDate = new Date();
    let selectedDate = new Date(this.deleteLogForm.value.toTime)
    let Difference_In_Time = currentDate.getTime() - selectedDate.getTime();
    let Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);

    if(Difference_In_Days <= 45){
      this.validateSelectedDate = 'The from date should be less than current date - 45 days!';
    }
    else{
      const model: DownloadLogModel = {
        listDevice: [this.deviceMachineForPingStatus],
        toTime: this.convertDateBeforeSendToServer(this.deleteLogForm.value.toTime)
      };
      
      this.attendanceClient.deleteLogOnDeviceFromToTime(model).subscribe((data: any) =>{
        if(data){
          const listError: Array<string> = data.data.listIpFailed;
          
          if(data.succeeded && listError.length == 0){
            this.notificationService.success("Delete log success !");
            this.showDialogDelete = false;
          }
          else{
            this.notificationService.error("Delete log failed.");
          }
        }
      });
    }
  }

  selectToTime(data){
    this.toTime = this.convertDateBeforeSendToServer(data.value).toString();
  }

  public convertDateBeforeSendToServer(dateTimeString) {
    const dateTimeObject = new Date(dateTimeString);
    return new Date(dateTimeObject.getTime() - dateTimeObject.getTimezoneOffset() * 60000);
  }
  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();

    if (this.ref) {
      this.ref.close();
    }
  }
}
