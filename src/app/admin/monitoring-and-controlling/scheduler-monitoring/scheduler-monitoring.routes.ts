import { Routes } from '@angular/router';
import { SchedulesLogComponent } from './scheduler-monitoring.component';

export const SchedulesLogRoutes: Routes = [{ path: '', component: SchedulesLogComponent }];
