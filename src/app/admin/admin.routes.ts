import { Routes } from '@angular/router';
import { UserRole } from 'app/shared/constants/user-role.constants';
import { AuthenticationGuard } from 'app/shared/guards/authentication.guard';
import { RoleGuard } from 'app/shared/guards/role.guard';
import { AdminComponent } from './admin.component';

export const adminRoutes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AuthenticationGuard],
    children: [
      {
        path: 'news/category',
        loadChildren: () => import('./news-management/category/category.module').then((m) => m.CategoryModule),
        canActivate: [AuthenticationGuard],
      },
      {
        path: 'news/normal-news',
        loadChildren: () => import('./news-management/normal-news/normal-news.module').then((m) => m.NormalNewsModule),
        canActivate: [AuthenticationGuard],
      },
      {
        path: 'news/recruitment',
        loadChildren: () => import('./news-management/recruitment/recruitment.module').then((m) => m.RecruitmentModule),
        canActivate: [AuthenticationGuard],
      },

      {
        path: 'applys',
        loadChildren: () => import('./apply-management/apply/apply.module').then((m) => m.ApplyModule),
        canActivate: [AuthenticationGuard],
      },
      // ERP Data Integration
      {
        path: 'attendance-summary',
        loadChildren: () => import('./erp-data-integration/attendance-summary/attendance-summary.module').then((m) => m.AttendanceSummaryModule),
        canActivate: [AuthenticationGuard],
      },
      {
        path: 'integration-history',
        loadChildren: () => import('./erp-data-integration/integration-history/integration-history.module').then((m) => m.IntegrationHistoryModule),
        canActivate: [AuthenticationGuard],
      },
      // Machines Data Integration
      {
        path: 'attendance-log',
        loadChildren: () => import('./machine-data-integration/attendance-log/attendance-log.module').then((m) => m.MovementRequestModule),
        canActivate: [AuthenticationGuard],
      },
      {
        path: 'purging-history',
        loadChildren: () => import('./machine-data-integration/purging-history/purging-history.module').then((m) => m.PurgchingHistoryModule),
        canActivate: [AuthenticationGuard],
      },
      // Monitoring & Controlling
      {
        path: 'machines-status',
        loadChildren: () => import('./monitoring-and-controlling/machines-status/machines-status.module').then((m) => m.MachineStatusModule),
        canActivate: [AuthenticationGuard],
      },
      {
        path: 'scheduler-monitoring',
        loadChildren: () => import('./monitoring-and-controlling/scheduler-monitoring/scheduler-monitoring.module').then((m) => m.SchedulesLogModule),
        canActivate: [AuthenticationGuard],
      },
      {
        path: 'errors-log',
        loadChildren: () => import('./monitoring-and-controlling/errors-log/errors-log.module').then((m) => m.ErrorsLogModule),
        canActivate: [AuthenticationGuard],
      },
      {
        path: 'manual-control',
        loadChildren: () => import('./monitoring-and-controlling/manual-control/manual-control.module').then((m) => m.ManualControlModule),
        canActivate: [AuthenticationGuard],
      },
      // System Configuration
      {
        path: 'devices-model',
        loadChildren: () => import('./system-configuration/devices-model/devices-model.module').then((m) => m.DeviceModelModule),
        canActivate: [AuthenticationGuard],
      },
      {
        path: 'scheduler-service',
        loadChildren: () => import('./system-configuration/scheduler-service/scheduler-service.module').then((m) => m.SchedulerServiceModule),
        canActivate: [AuthenticationGuard],
      },
      {
        path: 'scheduler-setting',
        loadChildren: () => import('./system-configuration/scheduler-setting/scheduler-setting.module').then((m) => m.SchedulerSettingModule),
        canActivate: [AuthenticationGuard],
      },
      {
        path: 'timer-setting',
        loadChildren: () => import('./system-configuration/timer-setting/timer-setting.module').then((m) => m.TimerSettingModule),
        canActivate: [AuthenticationGuard],
      },
      {
        path: 'code-generation',
        loadChildren: () => import('./system-configuration/code-generation/code-generation.module').then((m) => m.CodeGenerationModule),
        canActivate: [AuthenticationGuard],
      },
      {
        path: 'user-profile',
        loadChildren: () => import('./pages/user-profile/user-profile.module').then((m) => m.UserProfileModule),
        canActivate: [AuthenticationGuard],
      },
    ],
  },
];
