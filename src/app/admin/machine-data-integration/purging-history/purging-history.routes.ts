import { Routes } from '@angular/router';
import { PrintComponent } from 'app/shared/components/print/print.component';
import { PurgingHistoryComponent } from './purging-history.component';

export const purgingHistoryRoutes: Routes = [
  {
    path: '',
    component: PurgingHistoryComponent
  },
  {
    path: 'print',
    outlet: 'print',
    component: PrintComponent
  }];
