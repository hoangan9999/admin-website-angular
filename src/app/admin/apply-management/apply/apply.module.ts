import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'app/shared/shared.module';
import { ApplyComponent } from './apply.component';
import { RouterModule } from '@angular/router';
import { applyRoutes } from './apply.routes';

@NgModule({
  imports: [CommonModule, SharedModule, RouterModule.forChild(applyRoutes)],
  declarations: [ApplyComponent],
  exports: [ApplyComponent],
})
export class ApplyModule { }
