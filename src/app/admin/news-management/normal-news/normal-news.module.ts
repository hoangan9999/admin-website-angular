import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'app/shared/shared.module';
import { NormalNewsComponent } from './normal-news.component';
import { RouterModule } from '@angular/router';
import { normalNewsRoutes } from './normal-news.routes';

@NgModule({
  imports: [CommonModule, SharedModule, RouterModule.forChild(normalNewsRoutes)],
  declarations: [NormalNewsComponent],
  exports: [NormalNewsComponent],
})
export class NormalNewsModule { }
