import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { DeviceMachineClients, DeviceMachineModel } from 'app/shared/api-clients/attendancemanagement.client';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NotificationService } from 'app/shared/services/notification.service';
import { WidthColumn } from 'app/shared/configs/width-column';
import { TypeColumn } from 'app/shared/configs/type-column';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FilesClient, TemplateType } from 'app/shared/api-clients/files.client';
import { ImportService } from 'app/shared/services/import.service';
import { EventType } from 'app/shared/enumerations/import-event-type.enum';

@Component({
  templateUrl: './devices-model.component.html',
})
export class DeviceModelComponent implements OnInit, OnDestroy {
  title = 'Devices Model';

  selectedItem: DeviceMachineModel;
  products: DeviceMachineModel[] = [];

  editMode = false;
  deviceMachineForm: FormGroup;

  titleDialog: string;
  dialog = false;

  WidthColumn = WidthColumn;
  TypeColumn = TypeColumn;

  cols: any[] = [];
  fields: any[] = [];

  ref: DynamicDialogRef;
  destroyed$ = new Subject<void>();

  active = true;

  get seriNumberControl() {
    return this.deviceMachineForm.get('seriNumber');
  }

  get factoryNameControl() {
    return this.deviceMachineForm.get('factoryName');
  }

  get deviceCodeControl() {
    return this.deviceMachineForm.get('deviceCode');
  }

  get ipAddressControl() {
    return this.deviceMachineForm.get('ipAddress');
  }

  get portNumberControl() {
    return this.deviceMachineForm.get('portNumber');
  }

  get notesControl() {
    return this.deviceMachineForm.get('notes');
  }

  get activeCodeControl() {
    return this.deviceMachineForm.get('activeCode');
  }

  get activeControl() {
    if (this.deviceMachineForm.value.activeCode == null || this.deviceMachineForm.value.activeCode === '') {
      this.deviceMachineForm.get('active').setValue(false);
    }
    return this.deviceMachineForm.get('active');
  }

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private confirmationService: ConfirmationService,
    private dialogService: DialogService,
    private filesClient: FilesClient,
    private deviceMachineClient: DeviceMachineClients,
    private importService: ImportService
  ) {}

  ngOnInit() {
    this.cols = [
      { header: '', field: 'checkBox', width: WidthColumn.CheckBoxColumn, type: TypeColumn.CheckBoxColumn },
      { header: 'Index', field: '', width: WidthColumn.IdentityColumn, type: TypeColumn.IdentityColumn },
      { header: 'Factory Name', field: 'factoryName', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Device Code', field: 'deviceCode', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'IP', field: 'ipAddress', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Port Number', field: 'portNumber', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Seri Number', field: 'seriNumber', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      // { header: 'Active Code', field: 'activeCode', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Active', field: 'active', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      // { header: 'Notes', field: 'notes', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Updated By', field: 'lastModifiedBy', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Updated Time', field: 'lastModified', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
    ];

    this.fields = this.cols.map((i) => i.field);
    this.initForm();
    // this.initProducts();
    this.initDeviceMachines();
    this.initEventBroadCast();
  }

  initEventBroadCast() {
    this.importService.getEvent().subscribe((event) => {
      switch (event) {
        case EventType.HideDialog:
          if (this.ref) {
            this.ref.close();
          }
          this.initDeviceMachines();
          break;
      }
    });
  }

  initForm() {
    this.deviceMachineForm = this.fb.group({
      id: [0],
      seriNumber: ['', [Validators.required]],
      factoryName: ['', [Validators.required]],
      deviceCode: ['', [Validators.required]],
      ipAddress: ['', [Validators.required]],
      portNumber: ['', [Validators.required, Validators.min(0)]],
      notes: [''],
      activeCode: [''],
      active: null,
      lastModifiedBy: [''],
      lastModified: [null],
    });
  }

  initDeviceMachines() {
    this.deviceMachineClient
      .getDeviceMachines()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (data) => (this.products = data),
        (_) => (this.products = [])
      );
  }

  openCreateDialog() {
    this.deviceMachineForm.reset();
    this.titleDialog = 'Add New Device';
    this.editMode = false;
    this.dialog = true;
  }

  hideDialog() {
    this.dialog = false;
    this.deviceMachineForm.reset();
  }

  editProduct(deviceMachine: DeviceMachineModel) {
    this.titleDialog = 'Edit DeviceMachine';
    this.deviceMachineForm.patchValue(deviceMachine);
    this.editMode = true;
    this.dialog = true;
  }

  deleteProduct(deviceMachine: DeviceMachineModel) {
    this.confirmationService.confirm({
      message: 'Do you confirm to delete this item?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.deviceMachineClient
          .deleteDeviceMachineAysnc(deviceMachine.id)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(
            (result) => {
              if (result && result.succeeded) {
                this.notificationService.success('Delete DeviceMachine Successfully');
                this.selectedItem = null;
                this.initDeviceMachines();
              } else {
                this.notificationService.error(result?.error);
              }
            },
            (_) => this.notificationService.error('Delete DeviceMachine Failed. Please try again')
          );
      },
    });
  }

  onSubmit() {
    if (this.deviceMachineForm.invalid) {
      return;
    }

    this.editMode ? this.handleUpdateProduct() : this.handleAddProduct();
  }

  handleAddProduct() {
    const model = this.deviceMachineForm.value as DeviceMachineModel;
    model.id = 0;

    this.deviceMachineClient
      .addDeviceMachine(model)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (result) => {
          if (result && result.succeeded) {
            this.notificationService.success('Add DeviceMachine Successfully');
            this.initDeviceMachines();
            this.hideDialog();
          } else {
            this.notificationService.error(result?.error);
          }
        },
        (_) => this.notificationService.error('Add DeviceMachine Failed. Please try again')
      );
  }

  handleUpdateProduct() {
    const id = this.deviceMachineForm.value.id;
    this.deviceMachineClient
      .updateDeviceMachine(this.deviceMachineForm.value.id, this.deviceMachineForm.value)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (result) => {
          if (result && result.succeeded) {
            this.notificationService.success('Update DeviceMachine Successfully');
            this.initDeviceMachines();
            this.hideDialog();

            this.deviceMachineClient
              .getDeviceMachinesByKey(id)
              .pipe(takeUntil(this.destroyed$))
              .subscribe(
                (machine) => (this.selectedItem = machine),
                (_) => (this.selectedItem = null)
              );
          } else {
            this.notificationService.error(result?.error);
          }
        },
        (_) => this.notificationService.error('Update DeviceMachine Failed. Please try again')
      );
  }

  handleChangeActive(event) {
    // if(this.deviceMachineForm.value.activeCode === '' || this.deviceMachineForm.value.activeCode == null){
    //  this.deviceMachineForm.value.active = false
    // }
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();

    if (this.ref) {
      this.ref.close();
    }
  }
}
