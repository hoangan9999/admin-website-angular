import { Routes } from '@angular/router';
import { RecruitmentComponent } from './recruitment.component';

export const recruitmentRoutes: Routes = [{ path: '', component: RecruitmentComponent }];
