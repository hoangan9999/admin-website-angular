import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  IntegrationHistoryClients,
  IntegrationHistoryModel,
} from 'app/shared/api-clients/attendancemanagement.client';
import { TypeColumn } from 'app/shared/configs/type-column';
import { WidthColumn } from 'app/shared/configs/width-column';
import { HistoryDialogType } from 'app/shared/enumerations/history-dialog-type.enum';
import { EventType } from 'app/shared/enumerations/import-event-type.enum';
import { ApplicationUser } from 'app/shared/models/application-user';
import { AuthenticationService } from 'app/shared/services/authentication.service';
import { NotificationService } from 'app/shared/services/notification.service';
import { PrintService } from 'app/shared/services/print.service';
import { ConfirmationService } from 'primeng/api';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  templateUrl: './integration-history.component.html',
  styleUrls: ['./integration-history.component.scss'],
})
export class IntegrationHistoryComponent implements OnInit, OnDestroy {
  title = 'Erp Integration History';
  titleDialog = '';

  shippingMarkForm: FormGroup;

  user: ApplicationUser;
  integrationHistory: IntegrationHistoryModel[] = [];
  selectedIntegrationHistory: IntegrationHistoryModel;

  canRePrint = false;

  isEdit = false;
  isShowDialog = false;
  isShowDialogCreate = false;
  isShowDialogDetail = false;
  isShowDialogHistory = false;
  isShowDialogEdit = false;

  cols: any[] = [];
  fields: any[] = [];
  TypeColumn = TypeColumn;
  HistoryDialogType = HistoryDialogType;

  expandedItems: any[] = [];
  printData: any;

  private destroyed$ = new Subject<void>();

  constructor(
    public printService: PrintService,
    private integrationHistoryClients: IntegrationHistoryClients,
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private authenticationService: AuthenticationService,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {
    this.authenticationService.user$.pipe(takeUntil(this.destroyed$)).subscribe((user: ApplicationUser) => (this.user = user));
    this.canRePrint = this.printService.canRePrint(this.user);

    this.cols = [
      { header: '', field: 'checkBox', width: WidthColumn.CheckBoxColumn, type: TypeColumn.CheckBoxColumn },
      { header: 'Id', field: 'id', width: WidthColumn.IdentityColumn, type: TypeColumn.IdentityColumn },
      /*{ header: 'Notes', field: 'notes', width: WidthColumn.DescriptionColumn, type: TypeColumn.NormalColumn },
      { header: 'Updated By', field: 'lastModifiedBy', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Updated Time', field: 'lastModified', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
      { header: '', field: '', width: WidthColumn.IdentityColumn, type: TypeColumn.ExpandColumn },*/
    ];

    this.fields = this.cols.map((i) => i.field);

    this.initForm();
    this.initIntegrationHistory();
  }

  initForm() {
    this.shippingMarkForm = this.fb.group({
      id: [0],
      notes: [''],
      lastModifiedBy: [''],
      lastModified: [null],
      shippingRequest: [null, Validators.required],
      shippingMarkShippings: this.fb.array([]),
      receivedMarkPrintings: this.fb.array([]),
    });
  }


  initIntegrationHistory() {
    this.expandedItems = [];

    this.integrationHistoryClients
      .getIntegrationHistory()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (i) => (this.integrationHistory = i),
        (_) => (this.integrationHistory = [])
      );
  }

  onPrint() {
    this.printService.printDocument('shipping-mark');
  }

  openHistoryDialog() {
    this.isShowDialogHistory = true;
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }
}
