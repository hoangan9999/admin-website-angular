import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NotificationService } from 'app/shared/services/notification.service';
import { WidthColumn } from 'app/shared/configs/width-column';
import { TypeColumn } from 'app/shared/configs/type-column';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { EventType } from 'app/shared/enumerations/import-event-type.enum';
import { NewsClient, ApplyInfoDto } from 'app/shared/api-clients/news.client';
@Component({
  selector: 'app-apply',
  templateUrl: './apply.component.html',
  styleUrls: ['./apply.component.scss']
})
export class ApplyComponent implements OnInit {

  title = 'Thông Tin Ứng Viên';

  selectedItem: ApplyInfoDto;

  applyInfo: ApplyInfoDto[] = [];

  editMode = false;
  form: FormGroup;

  titleDialog: string;
  dialog = false;

  WidthColumn = WidthColumn;
  TypeColumn = TypeColumn;

  cols: any[] = [];
  fields: any[] = [];

  ref: DynamicDialogRef;
  destroyed$ = new Subject<void>();
  variableFlie: any;


  get jobTitleControl() {
    return this.form.get('jobTitle');
  }

  get candidateNameControl() {
    return this.form.get('candidateName');
  }

  get candidateEmailControl() {
    return this.form.get('candidateEmail');
  }

  get candidateNumberControl() {
    return this.form.get('candidateNumber');
  }

  get fileUrlControl() {
    return this.form.get('fileUrl');
  }



  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private confirmationService: ConfirmationService,
    private newsClient: NewsClient,
  ) { }

  ngOnInit() {
    this.cols = [
      { header: '', field: 'checkBox', width: WidthColumn.CheckBoxColumn, type: TypeColumn.CheckBoxColumn },
      { header: 'Index', field: '', width: WidthColumn.IdentityColumn, type: TypeColumn.IdentityColumn },
      { header: 'Tiêu Đề', field: 'jobTitle', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Tên Ứng Viên', field: 'candidateName', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Số ĐT', field: 'candidateNumber', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Ngày Apply', field: 'created', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
      { header: 'Updated Time', field: 'lastModified', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
    ];

    this.fields = this.cols.map((i) => {
      i.field
    });
    this.initForm();
    this.initApplys();
  }



  initForm() {
    this.form = this.fb.group({
      id: ['00000000-0000-0000-0000-000000000000'],
      jobTitle: [''],
      candidateName: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(30)]],
      cadidateEmail: ['', [Validators.required, Validators.email]],
      candidateNumber: ['', [Validators.required]],
      fileUrl: ['', [Validators.required]]
    });
  }

  initApplys() {
    this.newsClient
      .apiNewsApplyinfosGetallapplyinfo()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (data) => (this.applyInfo = data),
        (_) => (this.applyInfo = [])
      );
  }

  // openCreateDialog() {
  //   this.form.reset();
  //   this.titleDialog = 'Thêm Tin Tuyển Dụng';
  //   this.editMode = false;
  //   this.dialog = true;
  // }

  hideDialog() {
    this.dialog = false;
    this.form.reset();
  }

  editItem(appyInfo: ApplyInfoDto) {
    // this.titleDialog = ` Ứng Viên: ${appyInfo.candidateName} -- Vị Trí: ${appyInfo.jobTitle}`;
    // this.form.patchValue(appyInfo);
    // this.editMode = true;
    // this.dialog = true;
    window.open(appyInfo.fileUrl);
  }

  deleteItem(applyInfo: ApplyInfoDto) {
    this.confirmationService.confirm({
      message: 'Bạn có chắc xóa úng viên ' + applyInfo.candidateName + '--' + applyInfo.jobTitle,
      header: 'Xác Nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.newsClient
          .apiNewsApplyinfosDelete(applyInfo.id)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(
            (result) => {
              if (result && result.succeeded === true) {
                this.notificationService.success('Xóa Thành Công!');
                this.selectedItem = null;
                this.initApplys();
              } else {
                this.notificationService.error(result.errors[0]);
              }
            },
            (_) => this.notificationService.error('Có lỗi trong quá trình xóa, Vui lòng thử lại sau!')
          );
      },
    });
  }

  handleValueCkChange(value, title) {
    if (title === 'responsibilities') {
      this.form.patchValue({
        responsibilities: value,
      });
    }
    if (title === 'requirements') {
      this.form.patchValue({
        requirements: value,
      });
    }
    if (title === 'salaryAndBenefits') {
      this.form.patchValue({
        salaryAndBenefits: value,
      });
    }
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }

    this.editMode ? this.handleUpdate() : this.handleAdd();
  }

  handleAdd() {

  }

  handleUpdate() {
    const id = this.form.value.id;
    this.newsClient
      .apiNewsRecruitmentPut(this.form.value)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (result) => {
          if (result && result.succeeded === true) {
            this.notificationService.success('Chỉnh sửa thành công!');
            this.initApplys();
            this.hideDialog();
            this.newsClient
              .apiNewsRecruitmentGetrecruitmentbyid(id)
              .pipe(takeUntil(this.destroyed$))
              .subscribe(
                (data) => (this.selectedItem = data),
                (_) => (this.selectedItem = null)
              );
          }
          else if (result && result.succeeded === false) {
            this.notificationService.error(result.errors[0]);
          }
        },
        (_) => this.notificationService.error('Có lỗi trong quá trình chỉnh sửa, Vui lòng thử lại sau!')
      );
  }

  

  downloadCV() {
    // var request = new XMLHttpRequest();
    // request.open('GET', 'https://localhost:5002/CV/20_7_202152_1595180435TranDuyThai.pdf', true);
    // request.responseType = 'blob';
    // request.onload = function () {
    //   var reader = new FileReader();
    //   reader.readAsDataURL(request.response);
    //   reader.onload = function (e) {
    //     debugger;
    //     console.log('DataURL:', e.target.result);
    //     var file = dataURLtoFile(e.target.result, file);
    //     const url = window.URL.createObjectURL(new Blob([file]));
    //     const link = document.createElement('a');
    //     link.href = url;
    //     link.setAttribute('download', 'CV'); //or any other extension
    //     document.body.appendChild(link);
    //     link.click();

    //   };
    // };
    // request.send();
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();

    if (this.ref) {
      this.ref.close();
    }
  }

}

function dataURLtoFile(dataurl, filename) {
 
  var arr = dataurl.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), 
      n = bstr.length, 
      u8arr = new Uint8Array(n);
      
  while(n--){
      u8arr[n] = bstr.charCodeAt(n);
  }
  return new File([u8arr], filename, {type:mime});
}

