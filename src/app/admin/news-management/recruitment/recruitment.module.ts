import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'app/shared/shared.module';
import { RecruitmentComponent } from './recruitment.component';
import { RouterModule } from '@angular/router';
import { recruitmentRoutes } from './recruitment.routes';

@NgModule({
  imports: [CommonModule, SharedModule, RouterModule.forChild(recruitmentRoutes)],
  declarations: [RecruitmentComponent],
  exports: [RecruitmentComponent],
})
export class RecruitmentModule { }
