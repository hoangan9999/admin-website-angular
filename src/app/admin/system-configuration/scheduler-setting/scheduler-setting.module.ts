import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { SchedulerSettingComponent } from './scheduler-setting.component';
import { configurationRoutes } from './scheduler-setting.routes';

@NgModule({
  declarations: [SchedulerSettingComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(configurationRoutes)],
  exports: [SchedulerSettingComponent],
})
export class SchedulerSettingModule { }
