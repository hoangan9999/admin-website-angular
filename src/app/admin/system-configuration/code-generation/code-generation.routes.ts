import { Routes } from '@angular/router';
import { CodeGenerationComponent } from './code-generation.component';

export const codeGenerationRoutes: Routes = [{ path: '', component: CodeGenerationComponent }];
