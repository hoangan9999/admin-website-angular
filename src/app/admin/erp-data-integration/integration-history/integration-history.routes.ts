import { Routes } from '@angular/router';
import { IntegrationHistoryComponent } from './integration-history.component';
import { PrintComponent } from '../../../shared/components/print/print.component';

export const integrationHistoryRoutes: Routes = [
  {
    path: '',
    component: IntegrationHistoryComponent,
  },
  {
    path: 'print',
    outlet: 'print',
    component: PrintComponent,
  },
];
