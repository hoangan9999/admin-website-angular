import { Routes } from '@angular/router';
import { SchedulerSettingComponent } from './scheduler-setting.component';

export const configurationRoutes: Routes = [{ path: '', component: SchedulerSettingComponent }];
