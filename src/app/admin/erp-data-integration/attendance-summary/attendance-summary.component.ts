import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CommunicationClient } from 'app/shared/api-clients/communications.client';
import { AttendanceLogClients, AttendanceSummaryClients, AttendanceSummaryModel } from 'app/shared/api-clients/attendancemanagement.client';
import { TypeColumn } from 'app/shared/configs/type-column';
import { WidthColumn } from 'app/shared/configs/width-column';
import { HistoryDialogType } from 'app/shared/enumerations/history-dialog-type.enum';
import Utilities from 'app/shared/helpers/utilities';
import { NotificationService } from 'app/shared/services/notification.service';
import { ConfirmationService, SelectItem } from 'primeng/api';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  templateUrl: './attendance-summary.component.html',
  styleUrls: ['./attendance-summary.component.scss'],
})
export class AttendanceSummaryComponent implements OnInit, OnDestroy {
  title = 'Time Attendance Summary';

  attendanceSummary: AttendanceSummaryModel[] = [];

  selectedAttendanceSummary: AttendanceSummaryModel;

  shippingRequestForm: FormGroup;
  selectItems: SelectItem[] = [];

  isShowDialogCreate: boolean;
  isShowDialogHistory: boolean;
  isShowDialogDocuments: boolean;
  titleDialog = '';

  cols: any[] = [];
  fields: any[] = [];

  expandedItems: any[] = [];

  WidthColumn = WidthColumn;
  TypeColumn = TypeColumn;
  HistoryDialogType = HistoryDialogType;



  private destroyed$ = new Subject<void>();

  constructor(
    private attendanceSummaryClients: AttendanceSummaryClients,
    private confirmationService: ConfirmationService,
    private notificationService: NotificationService,
    private fb: FormBuilder,
    private communicationClient: CommunicationClient
  ) { }

  ngOnInit() {
    this.cols = [
      { header: '', field: 'checkBox', width: WidthColumn.CheckBoxColumn, type: TypeColumn.CheckBoxColumn },
      { header: 'Id', field: 'id', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      // // { header: 'Product Line', field: 'productLine', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      // { header: 'Bill To', field: 'billTo', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      // { header: 'Bill To Address', field: 'billToAddress', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      // { header: 'Ship To', field: 'shipTo', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      // { header: 'Ship To Address', field: 'shipToAddress', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      // { header: 'Account Number', field: 'accountNumber', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      // { header: 'Shipping Date', field: 'shippingDate', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
      // { header: 'Pickup Date', field: 'pickupDate', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
      // { header: 'Notes', field: 'notes', width: WidthColumn.DescriptionColumn, type: TypeColumn.NormalColumn },
      // { header: 'Status', field: 'status', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      // { header: 'Updated By', field: 'lastModifiedBy', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      // { header: 'Updated Time', field: 'lastModified', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
      // { header: '', field: '', width: WidthColumn.IdentityColumn, type: TypeColumn.ExpandColumn },
    ];

    this.fields = this.cols.map((i) => i.field);

    this.initForm();
    this.initAttendanceSumarry();
  }

  initAttendanceSumarry() {
    this.attendanceSummaryClients
      .getAttendanceSummary()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (i) => {
          this.attendanceSummary = i;
        },
        (_) => (this.attendanceSummary = [])
      );
  }

  initForm() {
    this.shippingRequestForm = this.fb.group({
      id: [0],
      notes: [''],
      pickupDate: [new Date()],
      customerName: [''],
      shippingDate: [new Date()],
      billTo: [''],
      billToAddress: [''],
      shipTo: [''],
      shipToAddress: [''],
      shippingRequestDetails: this.fb.array([]),
      lastModifiedBy: [''],
      lastModified: [null],
    });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }
}
