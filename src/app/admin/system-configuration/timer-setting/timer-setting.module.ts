import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { TimerSettingComponent } from './timer-setting.component';
import { configurationRoutes } from './timer-setting.routes';

@NgModule({
  declarations: [TimerSettingComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(configurationRoutes)],
  exports: [TimerSettingComponent],
})
export class TimerSettingModule { }
