import { NewsService } from 'app/shared/services/news.service';
import { FileParameter } from 'app/shared/api-clients/news.client';

export class UploadAdapter {
  private loader;
  constructor(loader: any, private service: NewsService) {
    this.loader = loader;
  }

  uploadFile(file) {
    const fileToUpload: FileParameter = {
      data: file,
      fileName: file.name,
    };

    return this.service.uploadImage(fileToUpload);
  }

  upload() {
    return this.loader.file.then(
      (file) =>
        new Promise((resolve, reject) => {
          this.uploadFile(file).subscribe(
            (result) => {
              resolve({ default: result['imageUrl'] });
            },
            (error) => {
              reject(error);
            }
          );
        })
    );
  }

  // upload() {
  //   return this.loader.file.then(
  //     (file) =>
  //       new Promise((resolve, reject) => {
  //         var myReader = new FileReader();
  //         myReader.onloadend = (e) => {
  //           resolve({ default: myReader.result });
  //         };

  //         myReader.readAsDataURL(file);
  //       })
  //   );
  // }
}
