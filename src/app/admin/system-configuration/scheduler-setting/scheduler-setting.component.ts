import { takeUntil } from 'rxjs/operators';
import { NotificationService } from 'app/shared/services/notification.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ConfigClients, SchedulerSettingModel, SchedulerSettingClients, DeviceMachineClients, DeviceMachineModel } from 'app/shared/api-clients/attendancemanagement.client';
import { WidthColumn } from 'app/shared/configs/width-column';
import { TypeColumn } from 'app/shared/configs/type-column';
import { Subject } from 'rxjs';
import { SchedulerSettingModule } from './scheduler-setting.module';
import { debugOutputAstAsTypeScript } from '@angular/compiler';

@Component({
  templateUrl: './scheduler-setting.component.html',
  styleUrls: ['./scheduler-setting.component.scss'],
})
export class SchedulerSettingComponent implements OnInit, OnDestroy {
  title = 'Scheduler Pool';

  schedulerSettings: SchedulerSettingModel[] = [];
  deviceMachine: DeviceMachineModel[] = [];
  selectedSchedulerSetting: SchedulerSettingModel;
  selectedDeviceMachine: SchedulerSettingModel[] = [];
  isShowEditDialog: boolean;
  isShowDeviceDialog: boolean;
  schedulerSettingForm: FormGroup;

  WidthColumn = WidthColumn;
  TypeColumn = TypeColumn;

  cols: any[] = [];
  deviceCols: any[] = [];
  colFields = [];
  selectedKey = '';
  mode = '';

  headerTitle = '';
  actualAssign = 0;

  get keyControl() {
    return this.schedulerSettingForm.get('key');
  }

  get descriptionsControl() {
    return this.schedulerSettingForm.get('descriptions');
  }

  get coverageQtyControl() {
    return this.schedulerSettingForm.get('coverageQty');
  }

  private destroyed$ = new Subject<void>();

  constructor(
    private configsClients: ConfigClients,
    private schedulerSettingClient: SchedulerSettingClients,
    private notifiactionService: NotificationService,
    private deviceMachineClient: DeviceMachineClients
  ) {}

  ngOnInit() {
    this.cols = [
      { header: '', field: 'checkBox', width: WidthColumn.CheckBoxColumn, type: TypeColumn.CheckBoxColumn },
      { header: 'Index', field: 'key', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Service Descriptions', field: 'descriptions', width: WidthColumn.DescriptionColumn, type: TypeColumn.NormalColumn },
      { header: 'Coverage Machine Qty', field: 'coverageQty', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Actual Assign', field: 'actualAssignQty', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      // { header: 'Updated By', field: 'lastModifiedBy', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      // { header: 'Updated Time', field: 'lastModified', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
    ];

    this.deviceCols = [
      { header: '', field: 'checkBox', width: WidthColumn.CheckBoxColumn, type: TypeColumn.CheckBoxColumn },
      { header: 'Factory Name', field: 'factoryName', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Device Code', field: 'deviceCode', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'IP', field: 'ipAddress', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Port Number', field: 'portNumber', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
    ];

    this.colFields = this.cols.map((i) => i.field);

    this.getSchedulerSettings();
    this.initForm();
  }

  initForm() {
    this.schedulerSettingForm = new FormGroup({
      key: new FormControl('', Validators.required),
      descriptions: new FormControl(''),
      coverageQty: new FormControl('', Validators.required),
    });
  }

  getSchedulerSettings() {
    this.schedulerSettingClient
      .getSchedulerSettings()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (schedulerSettings) => (this.schedulerSettings = schedulerSettings),
        (_) => (this.schedulerSettings = [])
      );
  }

  openEditDialog(config: SchedulerSettingModel) {
    this.headerTitle = "";
    this.isShowEditDialog = true;
    this.schedulerSettingForm.patchValue(config);
  }

  hideDeviceDialog() {
    this.mode = '';
    this.isShowDeviceDialog = false;
    this.schedulerSettingForm.reset();
    this.headerTitle = "";
  }

  /*onEdit() {
    const { key, coverageQty, descriptions, intervalTime } = this.schedulerSettingForm.value;

    const model: SchedulerSettingModel = {
      key,
      coverageQty,
      descriptions,
      intervalTime
    };

    this.schedulerSettingClient
      .updateSchedulerSetting(key, model)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (result) => {
          if (result && result.succeeded) {
            this.notifiactionService.success('Edit Scheduler Service Successfully');
            this.getSchedulerSettings();
            this.hideEditDialog();
            this.schedulerSettingClient
              .getSchedulerSettingsByKey(model.key)
              .pipe(takeUntil(this.destroyed$))
              .subscribe(
                (schedulerSetting) => (this.selectedSchedulerSetting = schedulerSetting),
                (_) => (this.selectedSchedulerSetting = null)
              );

          } else {
            this.notifiactionService.error(result.error);
          }
        },
        (_) => {
          this.notifiactionService.error('Edit Scheduler Service Falied. Please try again');
          this.hideEditDialog();
        }
      );
  }*/

  openDeviceDialog(item, mode) {
    this.mode = mode;
    if (mode == 'add') {
      this.selectedDeviceMachine = [];
      this.selectedKey = item.key;
      this.schedulerSettingClient
        .countSchedulerSettingByKey(item.key)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          (data) => (
            (this.actualAssign = data),
            (this.headerTitle = 'SCHEDULER SETTING | ' + ' KEY: ' + item.key + ' | CORVERAGE MACHINE QTY: ' + item.coverageQty + ' | ACTUAL ASSIGN: ' + this.actualAssign)
          ),
          (_) => (this.actualAssign = 0)
        );

      this.schedulerSettingClient
        .getDeviceList()
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          (data) => ((this.deviceMachine = data), console.log(this.deviceMachine.length)),
          (_) => (this.deviceMachine = [])
        );
    }

    if (mode == 'edit') {
      this.selectedKey = this.selectedSchedulerSetting.key;
      const key = this.selectedSchedulerSetting.key;
      this.schedulerSettingClient
      .countSchedulerSettingByKey(item.key)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (data) => (
          (this.actualAssign = data),
          (this.headerTitle = 'SCHEDULER SETTING | ' + ' KEY: ' + item.key + ' | CORVERAGE MACHINE QTY: ' + item.coverageQty + ' | ACTUAL ASSIGN: ' + this.actualAssign)
        ),
        (_) => (this.actualAssign = 0)
      );
      this.schedulerSettingClient
        .getDeviceListByKey(key)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          (data) => (
            (this.deviceMachine = data), (this.selectedDeviceMachine = data)
            ),
          (_) => (this.deviceMachine = [])
        );
    }

    this.isShowDeviceDialog = true;
  }

  addDevice() {
    const a = this.selectedDeviceMachine.map((x) => {
      return {
        key: this.selectedKey,
        ipAddress: x.ipAddress,
      };
    });

    if (this.mode == 'add') {
      this.schedulerSettingClient
        .addSchedeulSetting(a)
        .pipe(takeUntil(this.destroyed$))
        .subscribe((data) => (
          this.notifiactionService.success('Add devices sucess.'), 
          (this.isShowDeviceDialog = false),
          this.getSchedulerSettings()
          ));
    }

    if (this.mode == 'edit') {
      this.schedulerSettingClient
        .updateSchedulerSettingsByKey(this.selectedKey, a)
        .pipe(takeUntil(this.destroyed$))
        .subscribe((data) => (this.notifiactionService.success('Update devices sucess.'), (this.isShowDeviceDialog = false), this.getSchedulerSettings()));
    }
  }

  onSelected(data){
    console.log(data);
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }
}
