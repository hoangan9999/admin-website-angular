import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'app/shared/shared.module';
import { CategoryComponent } from './category.component';
import { RouterModule } from '@angular/router';
import { categoryRoutes } from './category.routes';

@NgModule({
  imports: [CommonModule, SharedModule, RouterModule.forChild(categoryRoutes)],
  declarations: [CategoryComponent],
  exports: [CategoryComponent],
})
export class CategoryModule { }
