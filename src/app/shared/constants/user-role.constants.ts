export const UserRole = {
  ShippingDept: 'ShippingDept',
  LogisticsDept: 'LogisticsDept',
  Manager: 'Manager',
  FAQDept: 'FAQDept',
  PlanningDept: 'PlanningDept',
  ITAdministrator: 'ITAdministrator',
  SystemAdministrator: 'SystemAdministrator',
};
