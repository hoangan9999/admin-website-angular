import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { purgingHistoryRoutes } from './purging-history.routes';
import { PurgingHistoryComponent } from './purging-history.component';

@NgModule({
  declarations: [PurgingHistoryComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(purgingHistoryRoutes)],
  exports: [PurgingHistoryComponent],
})
export class PurgchingHistoryModule {}
