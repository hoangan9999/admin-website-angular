import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NotificationService } from 'app/shared/services/notification.service';
import { WidthColumn } from 'app/shared/configs/width-column';
import { TypeColumn } from 'app/shared/configs/type-column';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { EventType } from 'app/shared/enumerations/import-event-type.enum';
import { NewsClient, NewsCategoryDto, NewsDto } from 'app/shared/api-clients/news.client';
@Component({
  selector: 'app-normal-news',
  templateUrl: './normal-news.component.html',

})
export class NormalNewsComponent implements OnInit {
  title = 'Tin Tức';

  selectedItem: NewsDto;
  news: NewsDto[] = [];
  categories: NewsCategoryDto[] = [];

  editMode = false;
  form: FormGroup;

  titleDialog: string;
  dialog = false;

  WidthColumn = WidthColumn;
  TypeColumn = TypeColumn;

  cols: any[] = [];
  fields: any[] = [];

  ref: DynamicDialogRef;
  destroyed$ = new Subject<void>();
  variableFlie: any;


  get titleControl() {
    return this.form.get('title');
  }

  get descriptionsControl() {
    return this.form.get('descriptions');
  }

  get imageUrlControl() {
    return this.form.get('imageUrl');
  }

  get categoryIdControl() {
    return this.form.get('categoryId');
  }

  get contentControl() {
    return this.form.get('content');
  }


  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private confirmationService: ConfirmationService,
    private newsClient: NewsClient,
  ) { }

  ngOnInit() {
    this.cols = [
      { header: '', field: 'checkBox', width: WidthColumn.CheckBoxColumn, type: TypeColumn.CheckBoxColumn },
      { header: 'Index', field: '', width: WidthColumn.IdentityColumn, type: TypeColumn.IdentityColumn },
      { header: 'Tiêu Đề', field: 'title', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Giới Thiệu', field: 'descriptions', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Hình Ảnh', field: 'imageUrl', width: WidthColumn.ImagesColumn, type: TypeColumn.ImagesColumn },
      { header: 'Loại Tin Tức', field: 'categoryName', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Nổi Bật', field: 'featured', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Create Time', field: 'created', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
      { header: 'Updated Time', field: 'lastModified', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
    ];

    this.fields = this.cols.map((i) => {
      i.field
    });
    this.initForm();
    this.initNews();
    this.initCategory();
  }



  initForm() {
    this.form = this.fb.group({
      id: ['00000000-0000-0000-0000-000000000000'],
      title: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(450)]],
      descriptions: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(450)]],
      imageUrl: [''],
      imageName: [''],
      categoryId: ['', [Validators.required]],
      featured: [false],
      content: [''],
      keyword: [''],
    });
  }

  initCategory() {
    this.newsClient
      .apiNewsNewcategoriesGetallnewscategory()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (data) => (this.categories = data),
        (_) => (this.categories = [])
      );
  }

  initNews() {
    this.newsClient
      .apiNewsNewsGetallnews()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (data) => (this.news = data),
        (_) => (this.news = [])
      );
  }

  openCreateDialog() {
    this.form.reset();
    this.titleDialog = 'Thêm Tin Tức';
    this.editMode = false;
    this.dialog = true;
  }

  hideDialog() {
    this.dialog = false;
    this.form.reset();
  }

  editItem(news: NewsDto) {
    this.titleDialog = 'Chỉnh Sửa Tin Tức';
    this.form.patchValue(news);
    this.editMode = true;
    this.dialog = true;
  }

  deleteItem(news: NewsDto) {
    debugger;
    this.confirmationService.confirm({
      message: 'Bạn có chắc xóa ' + news.title,
      header: 'Xác Nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.newsClient
          .apiNewsNewsDelete(news.id)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(
            (result) => {
              if (result && result == 1) {
                this.notificationService.success('Xóa Thành Công!');
                this.selectedItem = null;
                this.initNews();
              } else {
                this.notificationService.error('Có lỗi trong quá trình xóa, Vui lòng thử lại sau!');
              }
            },
            (_) => this.notificationService.error('Có lỗi trong quá trình xóa, Vui lòng thử lại sau!')
          );
      },
    });
  }

  onFileChange(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;

      reader.readAsDataURL(file);

      const fileToUpload = {
        data: file,
        fileName: file.name,
      };

      reader.onload = () => {
        this.form.patchValue({
          imageUrl: reader.result,
        });
      };

      this.variableFlie = fileToUpload;


      // this.newsClient.apiNewsNewsUploadimage(fileToUpload).subscribe(
      //   (result: any) => {
      //     if (result) {
      //       const { imageName, folderName, imageUrl } = result;
      //       this.form.patchValue({
      //         imageUrl: imageUrl,
      //       });

      //       this.form.patchValue({
      //         imageName: imageName,
      //       });
      //     }
      //   },
      //   (e) => {
      //     return '';
      //   }
      // );
    }
  }

  handleValueCkChange(value) {
    this.form.patchValue({
      content: value,
    });
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }

    this.editMode ? this.handleUpdate() : this.handleAdd();
  }

  handleAdd() {
    if(this.variableFlie != null && this.variableFlie != undefined){
      this.newsClient.apiNewsNewsUploadimage(this.variableFlie).subscribe(
        (result: any) => {
          if (result) {
            const { imageName, folderName, imageUrl } = result;
            this.form.patchValue({
              imageUrl: imageUrl,
            });
  
            this.form.patchValue({
              imageName: imageName,
            });
            const model = this.form.value as NewsDto;
            model.id = '00000000-0000-0000-0000-000000000000';
            if (model.featured == null) {
              model.featured = false
            }
            this.newsClient
              .apiNewsNewsPost(model)
              .pipe(takeUntil(this.destroyed$))
              .subscribe(
                (result) => {
                  if (result && result == 1) {
                    this.notificationService.success('Thêm Thành Công!');
                    this.initNews();
                    this.hideDialog();
                    this.variableFlie = null;
                  } else if (result && result == -1) {
                    this.notificationService.error('Tên loại đã tồn tại!');
                  }
                  else {
                    this.notificationService.error('Có lỗi trong quá trình thêm mới! Vui lòng thử lại sau!');
                  }
                },
                (_) => this.notificationService.error('Có lỗi trong quá trình thêm mới! Vui lòng thử lại sau!')
              );
          }
        }
      )
    }
    else{
      const model = this.form.value as NewsDto;
      model.id = '00000000-0000-0000-0000-000000000000';
      if (model.featured == null) {
        model.featured = false
      }
      this.newsClient
        .apiNewsNewsPost(model)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          (result) => {
            if (result && result == 1) {
              this.notificationService.success('Thêm Thành Công!');
              this.initNews();
              this.hideDialog();
            } else if (result && result == -1) {
              this.notificationService.error('Có lỗi trong quá trình thêm mới! Vui lòng thử lại sau!');
            }
            else {
              this.notificationService.error('Có lỗi trong quá trình thêm mới! Vui lòng thử lại sau!');
            }
          },
          (_) => this.notificationService.error('Có lỗi trong quá trình thêm mới! Vui lòng thử lại sau!')
        );
    }
    
  }

  handleUpdate() {
    const id = this.form.value.id;
    if(this.variableFlie != null && this.variableFlie != undefined){
      this.newsClient.apiNewsNewsUploadimage(this.variableFlie).subscribe(
        (result: any)=>{
          if (result) {
            const { imageName, folderName, imageUrl } = result;
            this.form.patchValue({
              imageUrl: imageUrl,
            });

            this.form.patchValue({
              imageName: imageName,
            });

            this.newsClient
            .apiNewsNewsPut(id,this.form.value)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
              (result) => {
                if (result && result == 1) {
                  this.notificationService.success('Chỉnh sửa thành công!');
                  this.initNews();
                  this.hideDialog();
                  this.variableFlie = null;
                  this.newsClient
                    .apiNewsNewsDetail(id)
                    .pipe(takeUntil(this.destroyed$))
                    .subscribe(
                      (data) => (this.selectedItem = data),
                      (_) => (this.selectedItem = null)
                    );
                }
                else if (result && result == -1) {
                  this.notificationService.error('Có lỗi trong quá trình chỉnh sửa, Vui lòng thử lại sau!');
                }
                else {
                  this.notificationService.error('Có lỗi trong quá trình chỉnh sửa, Vui lòng thử lại sau!');
                }
              },
              (_) => this.notificationService.error('Có lỗi trong quá trình chỉnh sửa, Vui lòng thử lại sau!')
            );
        }
      });
    }
    else{
      this.newsClient
      .apiNewsNewsPut(id, this.form.value)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (result) => {
          if (result && result == 1) {
            this.notificationService.success('Chỉnh sửa thành công!');
            this.initNews();
            this.hideDialog();
            this.newsClient
              .apiNewsNewsDetail(id)
              .pipe(takeUntil(this.destroyed$))
              .subscribe(
                (data) => (this.selectedItem = data),
                (_) => (this.selectedItem = null)
              );
          }
          else if (result && result == -1) {
            this.notificationService.error('Có lỗi trong quá trình chỉnh sửa, Vui lòng thử lại sau!');
          }
          else {
            this.notificationService.error('Có lỗi trong quá trình chỉnh sửa, Vui lòng thử lại sau!');
          }
        },
        (_) => this.notificationService.error('Có lỗi trong quá trình chỉnh sửa, Vui lòng thử lại sau!')
      );
    }
    
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();

    if (this.ref) {
      this.ref.close();
    }
  }

}
