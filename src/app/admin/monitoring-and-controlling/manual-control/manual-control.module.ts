import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ManualControlComponent } from './manual-control.component';
import { manualControlRoutes } from './manual-control.routes';

@NgModule({
  declarations: [ManualControlComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(manualControlRoutes)],
  exports: [ManualControlComponent],
})
export class ManualControlModule {}
