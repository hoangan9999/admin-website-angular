import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { AttendanceSummaryComponent } from './attendance-summary.component';
import { attendanceSummaryRoutes } from './attendance-summary.routes';

@NgModule({
  declarations: [AttendanceSummaryComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(attendanceSummaryRoutes)],
  exports: [AttendanceSummaryComponent],
})
export class AttendanceSummaryModule {}
