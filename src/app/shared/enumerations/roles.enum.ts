export const Roles = {
  SystemAdministrator: 'SystemAdministrator',
  ITAdministrator: 'ITAdministrator',
  ShippingDept: 'ShippingDept',
  Manager: 'Manager',
  LogisticsDept: 'LogisticsDept',
  PlanningDept: 'PlanningDept',
  FAQDept: 'FAQDept',
};
