import { takeUntil } from 'rxjs/operators';
import { NotificationService } from 'app/shared/services/notification.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ConfigClients, SchedulerSettingModel, SchedulerSettingClients } from 'app/shared/api-clients/attendancemanagement.client';
import { WidthColumn } from 'app/shared/configs/width-column';
import { TypeColumn } from 'app/shared/configs/type-column';
import { Subject } from 'rxjs';

@Component({
  templateUrl: './timer-setting.component.html',
  styleUrls: ['./timer-setting.component.scss'],
})
export class TimerSettingComponent implements OnInit, OnDestroy {
  title = 'Timer Setting';

  schedulerSettings: SchedulerSettingModel[] = [];
  selectedSchedulerSetting: SchedulerSettingModel;
  isShowEditDialog: boolean;
  schedulerSettingForm: FormGroup;

  WidthColumn = WidthColumn;
  TypeColumn = TypeColumn;

  cols: any[] = [];
  colFields = [];

  get keyControl() {
    return this.schedulerSettingForm.get('key');
  }

  get descriptionsControl() {
    return this.schedulerSettingForm.get('descriptions');
  }

  get intervalTimeControl() {
    return this.schedulerSettingForm.get('intervalTime');
  }

  private destroyed$ = new Subject<void>();

  constructor(private configsClients: ConfigClients, private schedulerSettingClient: SchedulerSettingClients, private notifiactionService: NotificationService) {}

  ngOnInit() {
    this.cols = [
      { header: '', field: 'checkBox', width: WidthColumn.CheckBoxColumn, type: TypeColumn.CheckBoxColumn },
      { header: 'Index', field: 'key', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Service Descriptions', field: 'descriptions', width: WidthColumn.DescriptionColumn, type: TypeColumn.NormalColumn },
      { header: 'Interval Time (mins)', field: 'intervalTime', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      // { header: 'Updated By', field: 'lastModifiedBy', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      // { header: 'Updated Time', field: 'lastModified', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
    ];

    this.colFields = this.cols.map((i) => i.field);

    this.getSchedulerSettings();
    this.initForm();
  }

  initForm() {
    this.schedulerSettingForm = new FormGroup({
      key: new FormControl('', Validators.required),
      descriptions: new FormControl(''),
      intervalTime: new FormControl('', Validators.required),
    });
  }

  getSchedulerSettings() {
    this.schedulerSettingClient
      .getSchedulerSettings()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (schedulerSettings) => (this.schedulerSettings = schedulerSettings),
        (_) => (this.schedulerSettings = [])
      );
  }

  openEditDialog(config: SchedulerSettingModel) {
    this.isShowEditDialog = true;
    this.schedulerSettingForm.patchValue(config);
  }

  hideEditDialog() {
    this.isShowEditDialog = false;
    this.schedulerSettingForm.reset();
  }

  /*onEdit() {
    const { key, intervalTime, descriptions } = this.schedulerSettingForm.value;

    const model: SchedulerSettingModel = {
      key,
      intervalTime,
      descriptions
    };

    this.schedulerSettingClient
      .updateTimerSetting(key, model)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (result) => {
          if (result && result.succeeded) {
            this.notifiactionService.success('Edit Scheduler Service Successfully');
            this.getSchedulerSettings();
            this.hideEditDialog();
            this.schedulerSettingClient
              .getSchedulerSettingsByKey(model.key)
              .pipe(takeUntil(this.destroyed$))
              .subscribe(
                (schedulerSetting) => (this.selectedSchedulerSetting = schedulerSetting),
                (_) => (this.selectedSchedulerSetting = null)
              );

          } else {
            this.notifiactionService.error(result.error);
          }
        },
        (_) => {
          this.notifiactionService.error('Edit Scheduler Service Falied. Please try again');
          this.hideEditDialog();
        }
      );
  }*/

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }
}
