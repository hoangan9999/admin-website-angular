import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'app/shared/shared.module';
import { MachineStatusComponent } from './machines-status.component';
import { RouterModule } from '@angular/router';
import { machineStatusRoutes } from './machines-status.routes';

@NgModule({
  imports: [CommonModule, SharedModule, RouterModule.forChild(machineStatusRoutes)],
  declarations: [MachineStatusComponent],
  exports: [MachineStatusComponent],
})
export class MachineStatusModule { }
