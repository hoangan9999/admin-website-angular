import { Injectable } from '@angular/core';
import { NewsClient, NewsDto, NewsCategoryDto, FileParameter } from 'app/shared/api-clients/news.client';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private client: NewsClient) {}

  getAllNewsCategory(companyIndex: number) {
    return this.client.apiNewsNewcategoriesGetallnewscategory();
  }

  addNewsCategory(newsCategory: NewsCategoryDto) {
    return this.client.apiNewsNewcategoriesPost(newsCategory);
  }

  editNewsCategory(id: string, newsCategory: NewsCategoryDto) {
    return this.client.apiNewsNewcategoriesPut(id, newsCategory);
  }

  deleteNewsCategory(id: string) {
    return this.client.apiNewsNewcategoriesDelete(id);
  }

  getAllNews(companyIndex: number) {
    return this.client.apiNewsNewsGetallnews();
  }

  addNews(news: NewsDto) {
    return this.client.apiNewsNewsPost(news);
  }

  editNews(id: string, news: NewsDto) {
    return this.client.apiNewsNewsPut(id, news);
  }

  deleteNews(id: string) {
    return this.client.apiNewsNewsDelete(id);
  }

  uploadImage(file: FileParameter) {
    return this.client.apiNewsNewsUploadimage(file);
  }
}
