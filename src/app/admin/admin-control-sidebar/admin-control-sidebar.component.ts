import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserRole } from 'app/shared/constants/user-role.constants';
import { ApplicationUser } from 'app/shared/models/application-user';
import { AuthenticationService } from 'app/shared/services/authentication.service';
import { MenuItem } from 'primeng/api';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-control-sidebar',
  templateUrl: './admin-control-sidebar.component.html',
})
export class AdminControlSidebarComponent implements OnInit, OnDestroy {
  items: MenuItem[];
  user: ApplicationUser;
  private destroyed$ = new Subject<void>();

  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.authenticationService.user$.pipe(takeUntil(this.destroyed$)).subscribe((user) => {
      if (user) {
        this.user = user;
      }
    });

    this.items = [
      // {
      //   label: 'ERP Data Integration',
      //   icon: 'pi pi-pw pi-file',
      //   items: [
      //     {
      //       label: 'Attendance Summary',
      //       icon: 'pi pi-envelope',
      //       routerLink: '/attendance-summary',
      //     },
      //     {
      //       label: 'Integration History',
      //       icon: 'pi pi-tags',
      //       routerLink: '/integration-history',
      //     },
      //   ],
      // },
      {
        label: 'Quản Lý Tin Tức',
        icon: 'pi pi-pw pi-file',
        items: [
          {
            label: 'Loại Tin Tức',
            icon: 'pi pi-calendar-plus',
            routerLink: '/news/category',
          },
          {
            label: 'Tin Tức',
            icon: 'pi pi-calendar-plus',
            routerLink: '/news/normal-news',
          },
          {
            label: 'Tin Tuyển Dụng',
            icon: 'pi pi-tags',
            routerLink: '/news/recruitment',
          },
        ],
      },
      {
        label: 'Quản Lý Ứng Viên',
        items: [
          {
            label: 'Apply',
            icon: 'pi pi-user-plus',
            routerLink: '/applys',
          },
        ],
      },
      
    ];
  }

  _viewedByRoles(roles: string[]): boolean {
    let haveAccess = false;
    this.user.roles.forEach((r) => {
      const result = roles.some((i) => i === r);

      if (result) {
        haveAccess = true;
      }
    });

    return haveAccess;
  }

  ngOnDestroy() {
    this.destroyed$.next();
    this.destroyed$.unsubscribe();
  }
}
