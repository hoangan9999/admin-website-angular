import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NotificationService } from 'app/shared/services/notification.service';
import { WidthColumn } from 'app/shared/configs/width-column';
import { TypeColumn } from 'app/shared/configs/type-column';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { EventType } from 'app/shared/enumerations/import-event-type.enum';
import { NewsClient, NewsCategoryDto } from 'app/shared/api-clients/news.client';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
})
export class CategoryComponent implements OnInit {
  title = 'Loại Tin Tức';

  selectedItem: NewsCategoryDto;
  categories: NewsCategoryDto[] = [];

  editMode = false;
  categoryForm: FormGroup;

  titleDialog: string;
  dialog = false;

  WidthColumn = WidthColumn;
  TypeColumn = TypeColumn;

  cols: any[] = [];
  fields: any[] = [];

  ref: DynamicDialogRef;
  destroyed$ = new Subject<void>();


  get categoryNameControl() {
    return this.categoryForm.get('categoryName');
  }

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private confirmationService: ConfirmationService,
    private newsClient: NewsClient,
  ) {}

  ngOnInit() {
    this.cols = [
      { header: '', field: 'checkBox', width: WidthColumn.CheckBoxColumn, type: TypeColumn.CheckBoxColumn },
      { header: 'STT', field: '', width: WidthColumn.IdentityColumn, type: TypeColumn.IdentityColumn },
      { header: 'Loại Tin Tức', field: 'categoryName', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Create Time', field: 'created', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
      { header: 'Updated Time', field: 'lastModified', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
    ];

    this.fields = this.cols.map((i) => i.field);
    this.initForm();
    this.initCategory();
  }

  

  initForm() {
    this.categoryForm = this.fb.group({
      id: ['00000000-0000-0000-0000-000000000000'],
      categoryName: ['', [Validators.required]]
    });
  }

  initCategory() {
    this.newsClient
      .apiNewsNewcategoriesGetallnewscategory()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (data) => (this.categories = data),
        (_) => (this.categories = [])
      );
  }

  openCreateDialog() {
    this.categoryForm.reset();
    this.titleDialog = 'Thêm Loại Tin Tức';
    this.editMode = false;
    this.dialog = true;
  }

  hideDialog() {
    this.dialog = false;
    this.categoryForm.reset();
  }

  editItem(category: NewsCategoryDto) {
    this.titleDialog = 'Chỉnh Sửa Loại Tin Tức';
    this.categoryForm.patchValue(category);
    this.editMode = true;
    this.dialog = true;
  }

  deleteItem(category: NewsCategoryDto) {
    debugger;
    this.confirmationService.confirm({
      message: 'Bạn có chắc xóa ' + category.categoryName,
      header: 'Xác Nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.newsClient
          .apiNewsNewcategoriesDelete(category.id)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(
            (result) => {
              if (result && result == 1) {
                this.notificationService.success('Xóa Thành Công!');
                this.selectedItem = null;
                this.initCategory();
              }
              else if(result && result == -1){
                this.notificationService.error('Tồn tại Tin Tức chứa loại tin tức này!');
              } 
              else {
                this.notificationService.error('Có lỗi trong quá trình xóa, Vui lòng thử lại sau!');
              }
            },
            (_) => this.notificationService.error('Có lỗi trong quá trình xóa, Vui lòng thử lại sau!')
          );
      },
    });
  }

  onSubmit() {
    if (this.categoryForm.invalid) {
      return;
    }

    this.editMode ? this.handleUpdate() : this.handleAdd();
  }

  handleAdd() {
    const model = this.categoryForm.value as NewsCategoryDto;
    model.id = '00000000-0000-0000-0000-000000000000';
    this.newsClient
      .apiNewsNewcategoriesPost(model)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (result) => {
          if (result && result == 1) {
            this.notificationService.success('Thêm Thành Công!');
            this.initCategory();
            this.hideDialog();
          }else if(result && result == -1){
            this.notificationService.error('Tên loại đã tồn tại!');
          } 
          else {
            this.notificationService.error('Có lỗi trong quá trình thêm mới! Vui lòng thử lại sau!');
          }
        },
        (_) => this.notificationService.error('Có lỗi trong quá trình thêm mới! Vui lòng thử lại sau!')
      );
  }

  handleUpdate() {
    const id = this.categoryForm.value.id;
    this.newsClient
      .apiNewsNewcategoriesPut(this.categoryForm.value.id, this.categoryForm.value)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (result) => {
          if (result && result == 1 ) {
            this.notificationService.success('Chỉnh sửa thành công!');
            this.initCategory();
            this.hideDialog();

            this.newsClient
              .apiNewsNewcategoriesDetails(id)
              .pipe(takeUntil(this.destroyed$))
              .subscribe(
                (machine) => (this.selectedItem = machine),
                (_) => (this.selectedItem = null)
              );
          } 
          else if(result && result == -1){
            this.notificationService.error('Tên loại đã tồn tại!');
          }
          else {
            this.notificationService.error('Có lỗi trong quá trình chỉnh sửa, Vui lòng thử lại sau!');
          }
        },
        (_) => this.notificationService.error('Có lỗi trong quá trình chỉnh sửa, Vui lòng thử lại sau!')
      );
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();

    if (this.ref) {
      this.ref.close();
    }
  }

}
