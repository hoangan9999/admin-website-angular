import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'app/shared/shared.module';
import { DeviceModelComponent } from './devices-model.component';
import { RouterModule } from '@angular/router';
import { deviceModelRoutes } from './devices-model.routes';

@NgModule({
  imports: [CommonModule, SharedModule, RouterModule.forChild(deviceModelRoutes)],
  declarations: [DeviceModelComponent],
  exports: [DeviceModelComponent],
})
export class DeviceModelModule { }
