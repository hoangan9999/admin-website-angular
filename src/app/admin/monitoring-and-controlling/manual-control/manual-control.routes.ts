import { Routes } from '@angular/router';
import { ManualControlComponent } from './manual-control.component';

export const manualControlRoutes: Routes = [{ path: '', component: ManualControlComponent }];
