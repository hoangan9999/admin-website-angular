import { Routes } from '@angular/router';
import { AttendanceSummaryComponent } from './attendance-summary.component';

export const attendanceSummaryRoutes: Routes = [{ path: '', component: AttendanceSummaryComponent }];
