import { Routes } from '@angular/router';
import { AttendanceLogComponent } from './attendance-log.component';

export const attendanceLogRoutes: Routes = [{ path: '', component: AttendanceLogComponent }];
