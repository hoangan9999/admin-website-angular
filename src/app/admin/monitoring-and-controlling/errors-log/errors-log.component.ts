import { ErrorLogsClients, ErrorDeviceInSchedulerJobModel } from 'app/shared/api-clients/schedule.client';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotificationService } from 'app/shared/services/notification.service';
import { ConfirmationService } from 'primeng/api';
import { WidthColumn } from 'app/shared/configs/width-column';
import { TypeColumn } from 'app/shared/configs/type-column';
import { HistoryDialogType } from 'app/shared/enumerations/history-dialog-type.enum';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FilesClient, TemplateType } from 'app/shared/api-clients/files.client';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { EventType } from 'app/shared/enumerations/import-event-type.enum';
import { ImportService } from 'app/shared/services/import.service';

@Component({
  selector: 'app-errors-log',
  templateUrl: './errors-log.component.html',
  styleUrls: ['./errors-log.component.scss'],
})
export class ErrorsLogComponent implements OnInit, OnDestroy {
  title = 'Errors Log';

  errorsLog: ErrorDeviceInSchedulerJobModel[] = [];

  selectedErrorsLog: ErrorDeviceInSchedulerJobModel;

  workOrderForm: FormGroup;

  isEdit = false;
  isShowDialogCreate = false;
  isShowDialogEdit = false;
  isShowDialogHistory = false;
  titleDialog = '';

  WidthColumn = WidthColumn;
  TypeColumn = TypeColumn;
  HistoryDialogType = HistoryDialogType;

  cols: any[] = [];
  fields: any[] = [];

  expandedItems: any[] = [];

  ref: DynamicDialogRef;
  private destroyed$ = new Subject<void>();

  constructor(
    private errorsLogClients: ErrorLogsClients,
    private confirmationService: ConfirmationService,
    private notificationService: NotificationService,
    private fb: FormBuilder,
    private dialogService: DialogService,
    private filesClient: FilesClient,
    private importService: ImportService
  ) { }

  ngOnInit() {
    this.cols = [
      //{ header: '', field: 'checkBox', width: WidthColumn.CheckBoxColumn, type: TypeColumn.CheckBoxColumn },
      { header: 'Index', field: '', width: WidthColumn.IdentityColumn, type: TypeColumn.IdentityColumn },
      { header: 'Job Id', field: 'ipAddress', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'IP Address', field: 'ipAddress', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Port Number', field: 'portNumber', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Error Audit At', field: 'errorAuditAt', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
      { header: 'Error Description', field: 'errorDescription', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
      { header: 'Is Handled', field: 'errorAuditAt', width: WidthColumn.DateColumn, type: TypeColumn.DateColumn },
      { header: 'Handled Time', field: 'errorDescription', width: WidthColumn.NormalColumn, type: TypeColumn.NormalColumn },
    ];

    this.fields = this.cols.map((i) => i.field);

    this.initErrorLogs();

  }


  exportTemplate() {
    this.filesClient
      .apiFilesExportTemplate(TemplateType.WorkOrder)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (i) => {
          const aTag = document.createElement('a');
          aTag.id = 'downloadButton';
          aTag.style.display = 'none';
          aTag.href = i;
          aTag.download = 'WorkOderTemplate';
          document.body.appendChild(aTag);
          aTag.click();
          window.URL.revokeObjectURL(i);

          aTag.remove();
        },
        (_) => this.notificationService.error('Failed to export template')
      );
  }

  initWorkOrderForm() {
    this.workOrderForm = this.fb.group({
      id: [0],
      refId: ['', [Validators.required]],
      notes: [''],
      lastModifiedBy: [''],
      lastModified: [null],
      workOrderDetails: this.fb.array([]),
    });
  }

  initErrorLogs() {
    this.expandedItems = [];

    this.errorsLogClients
      .getErrorLogs()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        (i) => (this.errorsLog = i),
        (_) => (this.errorsLog = [])
      );
  }




  openCreateDialog() {
    this.titleDialog = 'Create Work Order';
    this.isShowDialogCreate = true;
    this.isEdit = false;
  }

  hideDialog() {
    this.isShowDialogCreate = false;
    this.isShowDialogEdit = false;
    this.isShowDialogHistory = false;
    this.workOrderForm.reset();
  }


  openHistoryDialog() {
    this.isShowDialogHistory = true;
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();

    if (this.ref) {
      this.ref.close();
    }
  }
}
