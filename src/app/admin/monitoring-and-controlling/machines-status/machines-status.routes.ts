import { Routes } from '@angular/router';
import { MachineStatusComponent } from './machines-status.component';

export const machineStatusRoutes: Routes = [{ path: '', component: MachineStatusComponent }];
